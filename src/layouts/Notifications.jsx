import React, { useState, useEffect, useContext } from 'react';
import { Button, Offcanvas, Badge, Card, Row, Col } from 'react-bootstrap';
import { MdOutlineNotificationsNone } from "react-icons/md";
import { HiArchive } from "react-icons/hi";
import { IoArrowDownCircleSharp, IoArrowUpCircleSharp } from "react-icons/io5";
import { AiFillWarning, AiFillSafetyCertificate, AiFillExclamationCircle, AiFillAlert, AiFillHome } from "react-icons/ai";
import { http } from "../auth";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Contador from "./../components/Contador";
import { NotificationContext } from 'src/contexts';
import ReactTooltip from "react-tooltip";

const WS_URL =
  process.env.NODE_ENV == "production"
    ? "wss://" + "intranet.fibra360.net" + "/realtime/ws"
    : "ws://localhost:9051/realtime/ws";

const NotificationsCenter = ({ props }) => {
  const [show, setShow] = useState(false);
  const [liteMode, setLiteMode] = useState({});
  const [loading, setLoading] = useState(false);
  const { notifications, setNotifications } = useContext(NotificationContext);


  useEffect(() => {
    ReactTooltip.rebuild();
  });

  return (
    <>
      <Button variant="primary"
        style={{ fontSize: 20, padding: "7px 7px" }}
        onClick={() => setShow(true)} className="me-2">
        <span className="d-flex justify-content-end align-items-center">
          <MdOutlineNotificationsNone />
          <Badge bg="danger" style={{ fontSize: "12px" }} className='ms-2'>
            {notifications?.total > 99 ? "+99" : notifications?.total || 0}
          </Badge>
        </span>
      </Button>
      <Offcanvas show={show} placement={"end"} scroll={true} onHide={() => setShow(false)} {...props} style={{ overflowY: "scroll" }}>
        <Offcanvas.Header closeButton className='mb-2'>
          <Offcanvas.Title>
            <Link to={`/notificaciones`}>
              <Button className='eliminarB' style={{ display: "inline-block", paddingBottom: "8px" }}>
                <AiFillHome style={{ fontSize: 20, marginRight: "2px", color: "#3b3b3b" }} />
              </Button>
            </Link>
            <span>Notificaciones</span>


          </Offcanvas.Title>
        </Offcanvas.Header>

        {loading ?
          <svg
            className="spinner"
            width="65px"
            height="65px"
            viewBox="0 0 66 66"
            xmlns="http://www.w3.org/2000/svg"
            style={{
              margin: "5px auto",
              display: "block",
            }}
          >
            <circle
              className="path"
              fill="none"
              strokeWidth="6"
              strokeLinecap="round"
              cx="33"
              cy="33"
              r="30"
            ></circle>
          </svg>
          :
          <>
            <ReactTooltip effect="solid" place='top' />
            {notifications?.data && notifications?.data?.map((element, idx) => (
              <Card key={idx} className={
                (element?.theme == 'info' || element?.theme == 'default') ? "mx-auto notificationInfo" :
                  element?.theme == 'error' ? "mx-auto notificationError" :
                    element?.theme == 'warning' ? "mx-auto notificationAlert" :
                      element?.theme == 'success' ? "mx-auto notificationSuccess" : "mx-auto notificationInfo"
              } >
                <Row>
                  <Col xs={2} style={{ paddingLeft: "14px" }} className="d-flex justify-content-center align-items-top">
                    {(element?.theme == 'info' || element?.theme == 'default') && <AiFillExclamationCircle style={{ fontSize: 40 }} />}
                    {element?.theme == 'error' && <AiFillAlert style={{ fontSize: 40 }} />}
                    {element?.theme == 'warning' && <AiFillWarning style={{ fontSize: 40 }} />}
                    {element?.theme == 'success' && <AiFillSafetyCertificate style={{ fontSize: 40 }} />}
                  </Col>
                  <Col xs={10} style={{ paddingLeft: "0px" }} >
                    <div className='d-flex justify-content-between'>
                      <strong className="me-auto">
                        {(element?.theme == 'info' || element?.theme == 'default') && "Notificación"}
                        {element?.theme == 'error' && "Error"}
                        {element?.theme == 'warning' && "Alerta"}
                        {element?.theme == 'success' && "Confirmación"}
                      </strong>
                      <small>{<Contador fecha={new Date(element?.createdAt)} />}</small>
                    </div>
                    <div>
                      <Row>
                        <Col xs={9}>
                          <span style={!liteMode[idx] ? { marginTop: "2px", display: "inline-block" } : { whiteSpace: "pre-line", marginTop: "2px", display: "inline-block" }}>{!liteMode[idx] ? element?.content.substr(0, 23) + "..." : element?.content}</span>
                        </Col>
                        <Col xs={3} className='d-flex justify-content-end align-items-top'>
                          <Button className='eliminarB d-flex justify-content-end align-items-top'
                            data-tip={!liteMode[idx] ? "Expandir" : "Contraer"}
                            style={element?.theme == 'warning' ? { color: "#3b3b3b" } : {}}
                            onClick={() => { setLiteMode({ [idx]: !(liteMode[idx]) }) }}>
                            {!liteMode[idx] ?
                              <IoArrowDownCircleSharp style={{ fontSize: 20, marginRight: "2px" }} />
                              :
                              <IoArrowUpCircleSharp style={{ fontSize: 20, marginRight: "2px" }} />
                            }
                          </Button>
                          {element?._id &&
                            <Button className='eliminarB d-flex justify-content-end align-items-top'
                              disabled={element?.readAt}
                              style={element?.theme == 'warning' ? { color: "#3b3b3b" } : {}}
                              data-tip="Archivar (Marcar como leído)"
                              onClick={() => {
                                setLoading(true);
                                http
                                  .post(`/realtime/notifications/${element?._id}/`)
                                  .then((res) => {
                                    http({
                                      url: `/realtime/notifications`,
                                      method: "GET"
                                    }).then((res) => {
                                      setNotifications(res?.data)
                                      setLoading(false);
                                    })
                                      .catch((err) => {
                                        toast.error("Error al consultar notificaciones...")
                                        setLoading(false);
                                      });
                                  })
                                  .catch(err => {
                                    toast.error(
                                      "Hubo un problema al archivar..."
                                    );
                                    setLoading(false);
                                  })
                              }}
                            >
                              <HiArchive style={{ fontSize: 20, marginRight: "2px" }} />
                            </Button>
                          }
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </Card>
            ))}
          </>
        }
      </Offcanvas>
    </>
  );
};

export default NotificationsCenter;
