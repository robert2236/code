import React, { useState, useEffect } from "react";

import { Container, Navbar, Button } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import { useAuth, logout, http } from "../auth";
import { Navigate } from "react-router-dom";
import useWebSocket, { ReadyState } from "react-use-websocket";
import SidebarMenu from "../components/Sidebar";
import { isToday } from "../utils";
import NotificationsCenter from "./Notifications";
import { FaFileInvoiceDollar } from "react-icons/fa";
import ReactTooltip from "react-tooltip";
import { FiMenu, FiArrowLeft } from "react-icons/fi";

const WS_URL = "ws://localhost:9001/ws";

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <a
    href=""
    ref={ref}
    className="d-flex align-items-center text-white text-decoration-none dropdown-toggle"
    id="dropdownUser1"
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    <img
      src="https://github.com/mdo.png"
      alt=""
      width="32"
      height="32"
      className="rounded-circle me-2"
    />
    <strong>{children}</strong>
  </a>
));

const Layout = ({ notify = true, children, title, icon }) => {
  const [open, setOpen] = useState(false);
  const [logged, user] = useAuth();
  const [someBirthday, setSomeBirthday] = useState(false)
  const [notifications, setNotifications] = useState([])
  const [menu, setMenu] = useState(true)

  if (window.innerWidth < 900) {
    console.log('mobile')
}

  if (!logged) {
    return <Navigate to="/login" replace={true} />;
  }

  return (
    <div className="main bg-light">
      {/* <ToastContainer position="top-center" /> */}
      {menu &&
        <SidebarMenu showMenu={menu} setMenu={setMenu} />
      }

      <div style={{ width: "100%", overflowX: "auto" }}>
        <Navbar bg="white" className="shadow-sm">
        {!menu &&
          <Button
            style={{ color: '#0C1E35', padding: "5px" }}
            title={menu ? 'Ocultar menú' : 'Mostrar menú'}
            className="d-flex justify-content-center align-items-center ms-3 eliminarB customB"
            onClick={() => setMenu(!menu)}
          >
              <FiMenu style={{ fontSize: 26 }} />
          </Button>}
          <Container>
            <Navbar.Brand href="#home" style={user?.scopes.includes("asistente-virtual") ? { color: "#3d3d3d", fontSize: "30px" } : {}} className="d-flex justify-content-between align-items-center">
              {user?.scopes.includes("asistente-virtual") && title == "Pagos pendientes" ?
                <FaFileInvoiceDollar className="me-3" />
                :
                ""
              }
              <span className='ms-2'>{icon}</span> <span className="ms-2">{title}</span></Navbar.Brand>
          </Container>
          <Container className="d-flex justify-content-end me-2">
            {notify &&
              <NotificationsCenter notifications={notifications} />
            }
          </Container>
        </Navbar>
        <Container fluid className="p-3 p-md-5">
          {children}
        </Container>
      </div>
      {/* {someBirthday && (
        <Fireworks
          style={{
            position: "fixed",
            width: "100%",
            height: "100%",
            zIndex: "10000",
            pointerEvents: "none",
          }}
        />
      )} */}
    </div>
  );
};

export default Layout;
