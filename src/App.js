import { useEffect, useState, createContext } from "react";
import "./App.css";
import { Routes, Route, Link, Navigate, useLocation } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import useWebSocket, { ReadyState } from "react-use-websocket";
import { useAuth, http, logout } from "./auth";
import NotificacionesPage from "./pages/Notificaciones";
import HomePage from "./pages/home";
import LoginPage2 from "./pages/Login2";
import RecoverPage from "./pages/Recover";
import { NotificationContext } from "./contexts";
import LogPage from "./pages/Login";
import { set } from "react-hook-form";
import EstadisticasPage from "./pages/Estadisticas/Estadisticas";
import ConfiguracionPage from "./pages/Configuracion/Configuracion";
import OltPage from "./pages/Olt/Olt";
import UsuariosPage from "./pages/Usuarios/Usuarios";
import PruebaPage from "./pages/Prueba/Prueba";
import OnuPage from "./pages/Onu/OnuForm";
import OnuAutorizadas from "./pages/Onu/OnuAutorizadas";
import OnuUnconfigPage from "./pages/Onu/OnuUnconfig";

const WS_URL =
  process.env.NODE_ENV == "production"
    ? "wss://" + "intranet.fibra360.net" + "/realtime/ws"
    : "ws://localhost:9051/realtime/ws";

const UpdateText = () => (
  <div>
    Ya está disponible la nueva versión. Click <b>aqui</b> para actualizar o
    pulse <b>F5</b>
  </div>
);

export const FormSelectContext = createContext();

function App() {
  const [logged, user, tokenProvider] = useAuth();
  const [socketUrl, setSocketUrl] = useState(WS_URL);
  const [notifyTab, setNotifyTab] = useState(0);
  const [start, setStart] = useState(true);
  const [change, setChange] = useState(false);
  const [notifications, setNotifications] = useState({ data: [] });
  const { sendMessage, sendJsonMessage, lastMessage, readyState } = useWebSocket(socketUrl, {
    shouldReconnect: (closeEvent) => true,
    share: true
  });

  const location = useLocation();

  useEffect(() => {
    if (!document.hidden) {
      let payload = { 'value': location.pathname }
      sendMessage(`currentTab,${JSON.stringify(payload)}`)
    }
  }, [location, document.hidden])


  useEffect(() => {
    if (logged) {
      let access = tokenProvider.getTokenInternal().access_token
      setSocketUrl(`${WS_URL}?token=${access}`)
    }
    // setSocketUrl(
    //   `${WS_URL}?${user?.hasOwnProperty("username") ? "username=" + user.username : ""
    //   }`
    // );
  }, [user, logged]);

  useEffect(() => {
    if (window.Notification) {
      window.Notification.requestPermission()
    }
  }, [])

  useEffect(() => {
    if (user && logged) {
      http({
        url: `/realtime/notifications`,
        method: "GET"
      }).then((res) => {
        setNotifications(res.data)
        setNotifyTab(res.data.data.length)
      })
        .catch((err) => {
          toast.error("Error al consultar notificaciones...")
        });
    }
  }, [user])

  useEffect(() => {
    if (lastMessage != null) {
      let event = JSON.parse(lastMessage.data);
      if (event.type && event.type == "notification") {
        setStart(true)
        toast(event.content, {
          type: event.theme,
          theme: "colored",
          autoClose: false,
        });
        setNotifications(prev => ({
          data: [{
            type: 'notification',
            content: event.content,
            theme: event.theme,
            createdAt: new Date()
          }, ...prev.data]
        }));
        setChange(!change)
        setNotifyTab(notifyTab + 1)
        if (window.Notification && window.Notification.permission == 'granted') {
          let n = new Notification(event.msg, { tag: 'soManyNotification' })
        }
      }
      if (event.type && event.type == "update") {
        toast.info(UpdateText, {
          theme: "colored",
          autoClose: true,
          onClose: () => window.location.reload(),
        });

        if (window.Notification && window.Notification.permission == 'granted') {
          let n = new Notification(event.msg, { tag: 'soManyNotification' })
        }
      }
      if (event.type && event.type == 'logout') {
        toast.info('Su sesión ha caducado')
        logout()
      }
    }
  }, [lastMessage]);

  return (
    <>
      <ToastContainer position="top-center" />
      <NotificationContext.Provider value={{ notifications, setNotifications }}>
        <Routes>
          <Route path="/home" element={<HomePage />} />
          <Route path="/OnuForm" element={<OnuPage />} />
          <Route path="/OnuAutorizadas" element={<OnuAutorizadas />} />
          <Route path="/OnuUnconfig" element={<OnuUnconfigPage />} />
          <Route path="/Olt" element={<OltPage />} />
          <Route path="/Usuarios" element={<UsuariosPage />} />
          <Route path="/Estadisticas" element={<EstadisticasPage />} />
          <Route path="/Configuracion" element={<ConfiguracionPage />} />
          <Route path="/Prueba" element={<PruebaPage/>} />
          {/*<Route path="/herramientas/asist-stats" element={<StatsAsistPage />} />*/}
          <Route path="/notificaciones" element={<NotificacionesPage />} />
          {/* <Route path="/login" element={<LoginPage />} /> */}
          <Route path="/login" element={<LoginPage2 />} />
          <Route path="/Recover" element={<RecoverPage />} />
          <Route path="*" element={<Navigate to="/login" replace />} />
          <Route path="/Log" element={<LogPage />} />
        </Routes>
      </NotificationContext.Provider>
      
    </>
  );
}

export default App;
