import { useLocation } from "react-router-dom";
import { login, BASE_AUTH_URL } from "../auth";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";

const getCookie = () => {
  let name = "token=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  let ce = decodedCookie.split('token=');
  return ce[1];
}

const Log = () => {
    let navigate = useNavigate();
    const location = useLocation()
    const token = getCookie()

    axios.post('https://intranet.fibra360.net/api/verify-token', {}, { headers: {Authorization: `Bearer ${token}`} })
    .then(function (response) {
        console.log(response);
        login({user: response.data, access_token: token})
        navigate("/home")
        document.cookie = "token=; domain=fibra360.net; expires=Thu, 18 Dec 2013 12:00:00 UTC";
      })
      .catch(function (error) {
        console.log(error);
      });
};

export default Log;