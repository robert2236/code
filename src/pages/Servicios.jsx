import React, { useEffect, useState } from "react";
import Layout from "../layouts/Layout";
import {
  Card,
  Button,
  Popover,
  Overlay,
  Modal,
  Row,
  Col,
  ListGroup,
  Badge,
  ButtonGroup,
  InputGroup,
  Dropdown,
  Form,
  FormControl
} from "react-bootstrap";
import { useForm, Controller} from 'react-hook-form';
import Toast from "react-bootstrap/Toast";
import ToastContainer from "react-bootstrap/ToastContainer";
import { HiOutlineCalculator } from "react-icons/hi";
import ReactTooltip from "react-tooltip";
import { useTable } from "react-table";
import { IoMdArrowRoundBack } from "react-icons/io";
import { MdHomeWork } from 'react-icons/md';
import { useNavigate, useLocation } from "react-router-dom";
import { AiOutlineTool, AiOutlineUserAdd, AiOutlineUser, AiOutlineProfile, AiOutlineEye, AiOutlineUserSwitch, AiOutlineDollar } from "react-icons/ai";
import { FaFileInvoiceDollar, FaUserAltSlash, FaRegClipboard, FaSlidersH, FaRegUserCircle, FaRegAddressCard, FaRegCommentDots } from "react-icons/fa";
import { GrLocation, GrTechnology } from "react-icons/gr";
import { IoSettingsOutline } from "react-icons/io5";
import { BsChatDots } from "react-icons/bs";
import { toast } from "react-toastify";
import Pagination from "../components/Pagination";
import Table from "../components/Table";
import { useDebounce } from "./../hooks";
import { useParams } from "react-router-dom";
import DatosTecnicosModal from "../components/DatosTecnicosModal";
import FacturaServicioModal from "../components/FacturaServicioModal";
import ServicioEstadoModal from "../components/ServicioEstadoModal";
import CambiarPlanModal from "../components/CambiarPlan";
import ObservacionModal from "../components/ObservacionModal";
import { http, useAuth } from "../auth";
import CargarFactura from "../components/CargarFactura";
import VerLogCortesModal from "src/components/VerLogCortesModal";
import 'react-phone-number-input/style.css'
import PhoneInput, { isValidPhoneNumber } from 'react-phone-number-input'
import { AsyncPaginate } from "react-select-async-paginate";
import AsyncSelect from "react-select/async";
import { yupResolver } from '@hookform/resolvers/yup';



const CiudadesModal = (props) => {
  const { register, handleSubmit } = useForm();

  const loadCitiesOptions = (inputValue, callback) => {
    http.get(`/ciudades?q=${inputValue}`).then(({ data }) => {
      callback([
        //{ label: "ENVIAR A TODO EL ESTADO", value: "todas" },
        ...data.data.map((item) => ({ label: item.nombre, value: item.valor })),
      ]);
    });
  };
  const {
    control,
    watch,
    formState: { errors },
    reset,
    setValue,
  } = useForm({
    resolver: yupResolver()
  });

  
  const loadUrbsOptions = async (search, loadedOptions, { page }) => {
    if (watch("codigo_ciudad.")?.value != undefined) {
      const response = await http(`/urbanismos?q=${watch("codigo_ciudad.")?.value.toUpperCase()}&page=${page}&col=codigo_ciudad`);
      const responseJSON = response.data;
      const options = responseJSON.data.map((el) => ({
        label: el.nombre,
        value: el.valor,
      }));
      return {
        options: options,
        hasMore: responseJSON.has_next,
        additional: {
          page: page + 1,
        },
      }
    };
  };

  let codigo_ciudad = watch('codigo_ciudad')

  useEffect(() => {
    setValue('codigo_urbanismo', null)
  }, [codigo_ciudad])


  const onSubmit = (data) => {
    console.log(data);
    props.onHide();
  };
  return (
    <Modal size="lg" show={props.show} onHide={props.onHide}>
      <Modal.Header closeButton>
        <Modal.Title>Agregar Ubicación</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form id="form-observacion" onSubmit={handleSubmit(onSubmit)}>
          <Row>
            <Col className="mb-3 col-12">
              <Form.Label>Ciudad</Form.Label>
              <Controller
                name="codigo_ciudad"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <AsyncSelect
                  {...field}
                    onChange={(value) => {
                      field.onChange(value);
                      setValue("codigo_urbanismo", undefined);
                    }}
                    cacheOptions
                    defaultOptions
                    loadOptions={loadCitiesOptions}
                    placeholder="Seleccione..."
                    styles={{
                      menuPortal: (base) => ({
                        ...base,
                        zIndex: 9999,
                      }),
                    }}
                    menuPosition="absolute"
                    menuPortalTarget={document.body}
                  />
                )}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Label>Urbanismo</Form.Label>
              <Controller
               name="codigo_urbanismo"
              control={control}
              render={({ field }) => (
              <AsyncPaginate
              key={watch("codigo_ciudad")?.value}
              {...field}
              cacheOptions
              defaultOptions
             isClearable={true}
             additional={{
              page: 1,
            }}
            onChange={field.onChange}
            loadOptions={loadUrbsOptions}
            placeholder="Seleccione..."
            styles={{
             menuPortal: (base) => ({
              ...base,
              zIndex: 9999,
          }),
        }}
        menuPosition="absolute"
        menuPortalTarget={document.body}
      />
    )}
  />
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Label>Calle/Avenida</Form.Label>
              <FormControl {...register("calle")} placeholder="Inserte la calle o avenida..." />
            </Col>
            <Col>
              <Form.Label>Casa/Apartamento</Form.Label>
              <FormControl {...register("casa")} placeholder="Inserte la casa o apartamento..." />
            </Col>
          </Row>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button type="submit" form="form-observacion" variant="primary">
          Guardar
        </Button>
        <Button onClick={props.onHide} variant="secondary">
          Cerrar
        </Button>
      </Modal.Footer>
    </Modal>
  );
}  

const TransferirServicioNew = (props) => {

  var today = new Date().toISOString().split('T')[0];

  const {
    control,
    register,
    handleSubmit,
    watch,
    formState: { errors },
    setValue,
    reset,
  } = useForm();

  const defaultValues = {
    nombres: "",
    apellidos: "",
    fecha_nacimiento: "",
    cedula: '',
    cedula: '',
    id_zona: undefined,
    id_urbanismo: undefined,
    calle: "",
    casa_apto: "",
    codigo_ciudad: null,
    codigo_urbanismo: null,
    codigo_plan: "",
    telefono1: "",
    telefono2: "",
    correo: "",
    urbanismo: "",
  };

  const onHidden = () => {
    props.onHide();
    reset(defaultValues);
  };


  const loadCitiesOptions = (inputValue, callback) => {
    http.get(`/ciudades?q=${inputValue}`).then(({ data }) => {
      callback([
        ...data.data.map((item) => ({ label: item.nombre, value: item.valor })),
      ]);
    });
  };

  const loadUrbsOptions = async (search, loadedOptions, { page }) => {
    if (watch("codigo_ciudad.").value != undefined) {
      const response = await http(`/urbanismos/?q=${watch("codigo_ciudad")?.value.toUpperCase()}&page=${page}&col=codigo_ciudad`);
      const responseJSON = response.data;
      const options = responseJSON.data.map((el) => ({
        label: el.nombre,
        value: el.valor,
      }));
      return {
        options: options,
        hasMore: responseJSON.has_next,
        additional: {
          page: page + 1,
        },
      }
    };
  };
  const onSubmit = (data) => {
    data.nombre = data.nombres + " " + data.apellidos;
    data.zona_a_estado = "Aragua"
    data.zona_b_ciudad = data.codigo_ciudad.value
    data.zona_c_urbanismo = data.codigo_urbanismo.value
    data.direccion_corta = data.calle + " " + data.casa_apto
    data.zona_postal = "2117"
    data.email = data.correo
    data.rif = "261488000"

    http({
      url: `/servicios/${props.servicioId}/transferir`,
      method: "POST",
      data: data,
    })
      .then((res) => {
        toast.success('Su registro se ha realizado correctamente.')
        props.onUpdate()
      })
      .catch((err) =>
        toast.error(`Hubo un error al intentar su registro.\n Error: ${err.response?.data?.detail?.error}`));
  };

  let codigo_ciudad = watch('codigo_ciudad')

  useEffect(() => {
    setValue('codigo_urbanismo', null)
  }, [codigo_ciudad])

  return (
    <Modal size="xl" show={props.show} onHide={onHidden}>
      <Modal.Header closeButton>
        <Modal.Title>Ingrese los datos del cliente nuevo</Modal.Title>

      </Modal.Header>
      <Modal.Body>
        <form id="form" onSubmit={handleSubmit(onSubmit)} className="p-4">
          <Row>
            <Col>
              <Row>
                <span className="d-flex justify-content-center align-items-center" style={{ fontSize: "20px" }}> <FaRegUserCircle className="me-2" /> Datos Personales</span>
                <Form.Group className="mb-3" style={{ marginTop: "22px" }}>
                  <Form.Label>Nombres <span className="alerta">{errors.nombres?.message}</span></Form.Label>
                  <Form.Control type="text" {...register("nombres", { required: true })} />
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label>Apellidos <span className="alerta">{errors.apellidos?.message}</span></Form.Label>
                  <Form.Control type="text" {...register("apellidos", { required: true })} />
                </Form.Group>
              </Row>
            </Col>
            <Col>
              <span className="d-flex justify-content-center align-items-center" style={{ fontSize: "20px" }}> <FaRegAddressCard className="me-2" /> Nacionalidad:
                <span style={{ marginLeft: "12px", fontSize: "15px" }}>
                  <Form.Check
                    inline
                    label="V"
                    value="V"
                    type="radio"
                    id="inline-radio-1"
                    defaultChecked
                    {...register("nacionalidad", { required: true })}
                  />
                  <Form.Check
                    inline
                    label="E"
                    value="E"
                    type="radio"
                    id="inline-radio-1"
                    {...register("nacionalidad", { required: true })}
                  />
                </span>
              </span>
              &nbsp; &nbsp;
              <Form.Group className="mb-3">
                <Form.Label>Cédula de identidad <span className="alerta">{errors.cedula?.message}</span></Form.Label>
                <Form.Control type="text" {...register("cedula", { required: true })} />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Fecha de Nacimiento <span className="alerta">{errors.fecha_nacimiento?.message}</span></Form.Label>
                <Form.Control type="date" max={today} {...register("fecha_nacimiento", { required: true })} />
              </Form.Group>
            </Col>
          </Row>
          <span className="d-flex justify-content-center align-items-center mt-3 mb-3" style={{ fontSize: "20px" }}> <GrLocation className="me-2" /> Dirección del cliente</span>
          <Row>
            <Col>
              <Form.Group className="mb-3">
                <Form.Label>Ciudad <span className="alerta">{errors.fecha_nacimiento?.message}</span></Form.Label>
                <Controller
                  name="codigo_ciudad"
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <AsyncSelect
                      onChange={(value) => {
                        field.onChange(value);
                        setValue("codigo_urbanismo", undefined);
                      }}
                      cacheOptions
                      defaultOptions
                      loadOptions={loadCitiesOptions}
                      placeholder="Seleccione..."
                      styles={{
                        menuPortal: (base) => ({
                          ...base,
                          zIndex: 9999,
                        }),
                      }}
                      menuPosition="absolute"
                      menuPortalTarget={document.body}
                    />
                  )}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Urbanismo <span className="alerta">{errors.fecha_nacimiento?.message}</span></Form.Label>
                <Controller
                  name="codigo_urbanismo"
                  control={control}
                  render={({ field }) => (
                    <AsyncPaginate
                      key={watch("codigo_ciudad.")?.value}
                      {...field}
                      cacheOptions
                      defaultOptions
                      isClearable={true}
                      additional={{
                        page: 1,
                      }}
                      onChange={field.onChange}
                      loadOptions={loadUrbsOptions}
                      placeholder="Seleccione..."
                      styles={{
                        menuPortal: (base) => ({
                          ...base,
                          zIndex: 9999,
                        }),
                      }}
                      menuPosition="absolute"
                      menuPortalTarget={document.body}
                    />
                  )}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group className="mb-3">
                <Form.Label>Calle / Avenida <span className="alerta">{errors.calle?.message}</span></Form.Label>
                <Form.Control type="text" {...register("calle", { required: true })} />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Casa / Apartamento <span className="alerta">{errors.casa_apto?.message}</span></Form.Label>
                <Form.Control type="text" {...register("casa_apto", { required: true })} />
              </Form.Group>

            </Col>
          </Row>
          <span className="d-flex justify-content-center align-items-center mt-3 mb-3" style={{ fontSize: "20px" }}> <BsChatDots className="me-2" /> Datos de contacto</span>
          <Row>
            <Col>
              <div className="mb-3">
                <label htmlFor="telefono1" className="form-label">Teléfono Principal

                  <span className="alert">{errors.telefono1?.message}</span></label>
                <Controller
                  defaultValue=""
                  name="telefono1"
                  control={control}
                  rules={{
                    required: true,
                  }}

                  render={({ field }) => (
                    <PhoneInput

                      value={field.value}
                      onChange={(value) => {

                        if (value == undefined) {
                          field.onChange("")
                        } else { field.onChange(value); }

                      }}

                      defaultCountry="VE"
                      className="form-control" id="telefono1" />
                  )} />
              </div>
            </Col>
            <Col>
              <div className="mb-3">
                <label htmlFor="telefono2" className="form-label">Teléfono secundario <span className="alert">{errors.telefono2?.message}</span></label>
                <Controller
                  defaultValue=""
                  name="telefono2"
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <PhoneInput
                      value={field.value}
                      onChange={(value) => {

                        if (value == undefined) {
                          field.onChange("")
                        } else { field.onChange(value); }
                      }}
                      defaultCountry="VE"
                      className="form-control" id="telefono2" />
                  )} />
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <label htmlFor="email" className="form-label">Correo electrónico <span className="alert">{errors.correo?.message}</span></label>
              <input type="email" className="form-control" id="email" {...register("correo", { required: true })} />
            </Col>
          </Row>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onHidden} variant="outline-secondary">
          Cancelar
        </Button>
        <Button type="submit" form="form" variant="primary">
          Transferir
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

const TransferirServicioOld = (props) => {
  const [data, setData] = useState({
    data: [],
    has_prev: false,
    has_next: false,
    pages: 0,
    total: 0,
    pageSize: 0,
  });
  const [query, setQuery] = useState("");
  const debouncedSearchTerm = useDebounce(query, 500);

  let cancelToken;

  const search = (url) => {
    if (typeof cancelToken != typeof undefined) {
      cancelToken.abort();
    }

    cancelToken = new AbortController();

    return http.get(url, {
      signal: cancelToken.signal,
    });
  };

  const fetchData = async () => {
    try {
      let response = await search(
        `/usuarios?estado=SOLICITUD&q=${query}&col=cedula`
      );
      setData(response.data);
    } catch (error) { }
  };

  useEffect(() => {
    fetchData();
  }, [debouncedSearchTerm]);

  const onHidden = () => {
    props.onHide();
    setQuery(null)
  };



  const columns = React.useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id",
        width: "5%",
        Cell: ({ state, ...val }) => {
          return val.row.original.id_usuario
        },
      },
      {
        Header: "Nombres",
        accessor: "nombre",
      },
      {
        Header: "Cédula",
        accessor: "cedula",
        width: "20%"
      },
      {
        Header: "Acciones",
        accessor: "id_usuario",
        width: "17%",
        Cell: ({ row }) => {

          const TransferirUser = () => {
            var data = {}
            data.id_usuario = row.original.id_usuario

            http({
              url: `/servicios/${props.servicioId}/transferir`,
              method: "POST",
              data: data,
            })
              .then((res) => {
                toast.success('Su registro se ha realizado correctamente.')
                props.onUpdate()
              })
              .catch((err) =>
                toast.error(`Hubo un error al intentar su registro.\n Error: ${err.response?.data?.detail?.error}`));
          };

          return (
            <>

              <Button type="submit" form="form" variant="primary"
                onClick={() => {
                  TransferirUser();
                }}
              >
                <AiOutlineUserSwitch className="me-1" />Transferir
              </Button>
            </>
          )
        },
      },
    ],
    [data]
  );

  const hiddenColumns = React.useMemo(
    () =>
      !!localStorage.getItem("servicios-hidden")
        ? JSON.parse(localStorage.getItem("servicios-hidden"))
        : columns.map((column) => {
          if (column.show === false) return column.accessor || column.id;
        }),
    []
  );

  const table = useTable({
    columns,
    data: data?.data,
    initialState: {
      hiddenColumns: hiddenColumns,
    },
  });

  return (
    <Modal size="lg" show={props.show} onHide={onHidden}>
      <Modal.Header closeButton>
        <Modal.Title>Seleccione al cliente</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col className="mb-4 mt-2">
            <InputGroup>
              <input
                className="form-control form-control-lg mx-5 my-2"
                type="search"
                placeholder="Ingrese el número de cédula del cliente..."
                aria-label=""
                onChange={(e) => {
                  setQuery(e.target.value);
                }}
              />
            </InputGroup>
          </Col>
        </Row>
        {data?.data.length >= 1 ?
          <Table table={table} />
          :
          <div className="text-center mt-2 mb-4"><h4 className="d-flex justify-content-center align-items-center"><FaUserAltSlash className="me-2" />Usuario no encontrado</h4></div>

        }
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onHidden} variant="outline-secondary">
          Cancelar
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

const Servicios = () => {
  const location = useLocation();
  const [data, setData] = useState({
    data: [],
    has_prev: false,
    has_next: false,
    pages: 0,
    total: 0,
    pageSize: 0,
    user: {},
    blocked: false,
  });
  const navigate = useNavigate();
  const [AllColumnsHidden, setCollumnsHidden] = useState([]);
  const [selectedColumn, setSelectedColumn] = useState("__all__");
  const target = React.useRef(null);
  const [show, setShow] = useState(false);
  const [page, setCurrentPage] = useState(1);
  const [query, setQuery] = useState("");
  const debouncedSearchTerm = useDebounce(query, 500);
  const [modalShow, setModalShow] = useState(false);
  const [modalShowFactura, setModalShowFactura] = useState(false);
  const [showTransferirNew, setShowTransferirNew] = useState(false);
  const [showTransferirOld, setShowTransferirOld] = useState(false);
  const [servicioIdActive, setCurrentServicioId] = useState(null);
  const [modalShowServicio, setModalShowServicio] = useState(false);
  const [modalShowPlan, setModalShowPlan] = useState(false);
  const [modalLog, setModalLog] = useState(false);
  const [modalShowCargarFactura, setModalShowCargarFactura] = useState(false);
  const [modalShowObservacion, setModalShowObservacion] = useState(false);
  const [modalShowCiudad, setModalShowCiudad] = useState(false);
  const toastId = React.useRef(null);
  const [logged, user, tokenProvider] = useAuth();
  const [loading, setLoading] = useState(false);
  

  

  const params = useParams();

  useEffect(() => {
    ReactTooltip.rebuild();
  });

  const columns = React.useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id_servicio",
        show: false,
        width: "5%",
      },
      {
        Header: "Tipo de Servicio",
        accessor: "tipo_servicio",
      },
      {
        Header: "Dirección de Servicio",
        accessor: "direccion_servicio",
      },
      {
        Header: "Coordenadas",
        accessor: "coordenadas",
        show: false,
      },
      {
        Header: "Zona Estado",
        accessor: "zonaEstado",
        show: false,
      },
      {
        Header: "Zona Ciudad",
        accessor: "zonaCiudad",
        show: false,
      },
      {
        Header: "Zona Urbanismo",
        accessor: "zonaUrbanismo",
        show: false,
      },
      {
        Header: "Zona Postal",
        accessor: "zona_postal",
        show: false,
      },
      {
        Header: "Estado",
        accessor: "estatus",
        Cell: ({ row, value }) => {
          if (row.original.historico_plan.tipo_pago) {
            return <Badge bg="secondary">EXONERADO</Badge>;
          }
          if (value == "ACTIVO") {
            return <Badge bg="success">ACTIVO</Badge>;
          } if (value == "TRANSFERIDO") {
            return <Badge bg="dark">TRANSFERIDO</Badge>;
          } else if (value == "SUSPENDIDO") {
            return <Badge bg="warning">SUSPENDIDO</Badge>;
          } else if (value == "RETIRADO") {
            return <Badge bg="danger">RETIRADO</Badge>;
          } else {
            return value;
          }
        },
      },
      {
        Header: "Plan Actual",
        accessor: "historico_plan.plan.nombre",
      },
      {
        Header: "Dia de Corte",
        accessor: "historico_plan.fecha_corte",
        Cell: ({ value }) => new Date(value).getUTCDate(),
      },
      {
        Header: "Prox. Fecha de Pago",
        accessor: "historico_plan.fecha_pago",
      },
      {
        Header: "Fecha de instalación",
        accessor: "fecha_creacion",
      },
      {
        Header: "Acciones",
        accessor: "id_usuario",
        width: "170px",
        Cell: ({ row }) => {
          console.log(row.original)     
          
         return <>
          
            <ButtonGroup>
              <Dropdown>
                <Dropdown.Toggle variant="outline-primary"
                  style={{ fontSize: 16, padding: "0.15rem 0.34rem", borderTopRightRadius: "0%", borderBottomRightRadius: "0%", borderRight: 0 }}
                  id="dropdown-basic"
                  data-tip="Opciones">
                  <IoSettingsOutline className="m-1" />
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  {row.original.estatus != "TRANSFERIDO" &&
                    <>
                    {!user?.scopes.includes("asistente-virtual") &&
                    <>
                      <Dropdown.Item onClick={() => {
                        setCurrentServicioId(row.values.id_servicio);
                        setModalShowPlan(true);
                      }}>
                        <FaSlidersH className="me-1" style={{ fontSize: 17 }} />
                        Cambiar plan
                      </Dropdown.Item>
                      <Dropdown.Item onClick={() => {
                        setCurrentServicioId(row.values.id_servicio);
                        setModalShowServicio(true);
                      }}>
                        <HiOutlineCalculator className="me-1" style={{ fontSize: 17 }} />
                        Cambiar estado del servicio
                      </Dropdown.Item>
                      </>
                    }
                    </>
                  }
{!user?.scopes.includes("asistente-virtual") &&
                  <Dropdown.Item
                    onClick={() => {
                      setCurrentServicioId(row.values.id_servicio);
                      setModalShow(true);
                    }}
                  ><GrTechnology className="me-1" style={{ fontSize: 17 }} />
                    Ver detalles técnicos
                  </Dropdown.Item>
          }
                  <Dropdown.Item
                    onClick={() => {
                      setCurrentServicioId(row.values.id_servicio);
                      setModalShowObservacion(true);
                    }}
                  ><FaRegCommentDots className="me-1" style={{ fontSize: 17 }} />
                    Observacion
                  </Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => {
                      setCurrentServicioId(row.values.id_servicio);
                      setModalShowCiudad(true);
                    }}
                  ><MdHomeWork className="me-1" style={{ fontSize: 17 }} />
                    Ver Ciudades
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
              
              <Dropdown>
                <Dropdown.Toggle variant="outline-primary"
                  style={row.original.estatus != "TRANSFERIDO" ?
                    ({
                      fontSize: 16,
                      padding: "0.15rem 0.34rem",
                      borderBottomLeftRadius: "0%",
                      borderTopLeftRadius: "0%",
                      borderTopRightRadius: "0%",
                      borderBottomRightRadius: "0%",
                      borderRight: 0
                    })
                    :
                    ({
                      fontSize: 16,
                      padding: "0.15rem 0.34rem",
                      borderBottomLeftRadius: "0%",
                      borderTopLeftRadius: "0%"
                    })
                  }
                  id="dropdown-basic"
                  data-tip="Facturación">
                  <AiOutlineDollar className="m-1" />
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  {row.original.estatus != "TRANSFERIDO" &&
                    <>
                      {!user?.scopes.includes("asistente-virtual") &&
                        <Dropdown.Item onClick={() => {
                          setCurrentServicioId(row.values.id_servicio);
                          setModalShowCargarFactura(true);
                        }}>
                          <FaFileInvoiceDollar className="me-1" style={{ fontSize: 17 }} />
                          Cargar Factura
                        </Dropdown.Item>
                      }
                    </>
                  }
                  <Dropdown.Item onClick={() => {
                    setCurrentServicioId(row.values.id_servicio);
                    setModalShowFactura(true);
                  }}>
                    <FaRegClipboard className="me-1" style={{ fontSize: 17 }} />
                    Ver facturas
                  </Dropdown.Item>
                  <Dropdown.Item onClick={() => {
                    setCurrentServicioId(row.values.id_servicio);
                    setModalLog(true);
                  }}>
                    <AiOutlineProfile className="me-1" style={{ fontSize: 17 }} />
                    Ver historico de cortes
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>

              {row.original.estatus != "TRANSFERIDO" &&
              <>
              {!user?.scopes.includes("asistente-virtual") &&
                <Dropdown>
                  <Dropdown.Toggle variant="outline-primary"
                    style={{ fontSize: 16, padding: "0.15rem 0.34rem", borderBottomLeftRadius: "0%", borderTopLeftRadius: "0%" }}
                    id="dropdown-basic"
                    data-tip="Transferir servicio">
                    <AiOutlineUserSwitch className="m-1" />
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item
                      onClick={() => {
                        setCurrentServicioId(row.values.id_servicio);
                        setShowTransferirNew(true);
                      }}
                    ><AiOutlineUserAdd className="me-1" style={{ fontSize: 17 }} />Transferir a usuario nuevo</Dropdown.Item>
                    <Dropdown.Item
                      onClick={() => {
                        setCurrentServicioId(row.values.id_servicio);
                        setShowTransferirOld(true);
                      }}
                    ><AiOutlineUser className="me-1" style={{ fontSize: 17 }} />Transferir a usuario existente</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              }
              </>
              }
            </ButtonGroup>
            
          </>
        },
      },
    ],
    [data]
  );

  const hiddenColumns = React.useMemo(
    () =>
      !!localStorage.getItem("servicios-hidden")
        ? JSON.parse(localStorage.getItem("servicios-hidden"))
        : columns.map((column) => {
          if (column.show === false) return column.accessor || column.id;
        }),
    []
  );

  const table = useTable({
    columns,
    data: data.data,
    initialState: {
      hiddenColumns: hiddenColumns,
    },
  });

  const notify = () => {
    toastId.current = toast.error(
      "Este usuario se encuentra deshabilitado para facturar ya que cuenta con un pago pendiente",
      {
        autoClose: false,
      }
    );
  };

  const dismiss = () => toast.dismiss(toastId.current);

  const fetchData = async () => {
    setLoading(true);
    setData((prev) => ({ ...prev, data: [] }));
    let response = await http.get(
      `/servicios?page=${page}&q=${query}&col=${selectedColumn}&user_id=${params.userId}`
    );
    setData(response.data);
    if (response.data.blocked) {
      // alert('Este usuario se encuentra deshabilitado para cargar factura ya que tiene un pago pendiente')
    }
    setLoading(false);
  };

  const onHidden = () => setModalShow(false);

  useEffect(() => {
    if (data.blocked) {
      notify();
    }
    return () => {
      dismiss();
    };
  }, [data]);

  useEffect(() => {
    fetchData();
  }, [page]);

  useEffect(() => {
    setCurrentPage(1);
    if (query) {
      fetchData();
    }
  }, [debouncedSearchTerm]);

  useEffect(() => {
    localStorage.setItem(
      "servicios-hidden",
      JSON.stringify(table.state.hiddenColumns)
    );
  }, [table.state.hiddenColumns]);



  return (
    <Layout title="Servicios">
      <ReactTooltip effect="solid" className="up" />

    

      <DatosTecnicosModal
        servicioId={servicioIdActive}
        onHide={onHidden}
        show={modalShow}
      />
      <FacturaServicioModal
        servicioId={servicioIdActive}
        onHide={() => setModalShowFactura(false)}
        show={modalShowFactura}
      />

      <VerLogCortesModal
        servicioId={servicioIdActive}
        onHide={() => setModalLog(false)}
        show={modalLog}
      />
      <ServicioEstadoModal
        servicioId={servicioIdActive}
        show={modalShowServicio}
        onHide={() => setModalShowServicio(false)}
        onUpdate={fetchData}
      />

      <CambiarPlanModal
        servicioId={servicioIdActive}
        show={modalShowPlan}
        onHide={() => setModalShowPlan(false)}
        onUpdate={fetchData}
      />

      <ObservacionModal
        servicioId={servicioIdActive}
        show={modalShowObservacion}
        onHide={() => setModalShowObservacion(false)}
        onUpdate={fetchData}
      />
       <CiudadesModal
        servicioId={servicioIdActive}
        show={modalShowCiudad}
        onHide={() => setModalShowCiudad(false)}
        onUpdate={() => {
          setModalShowCiudad(false);
          fetchData()
        }}
      />


      <CargarFactura
        servicioId={servicioIdActive}
        show={modalShowCargarFactura}
        onHide={() => setModalShowCargarFactura(false)}
      />

      <TransferirServicioNew
        servicioId={servicioIdActive}
        show={showTransferirNew}
        onHide={() => setShowTransferirNew(false)}
        onUpdate={() => {
          setShowTransferirOld(false);
          fetchData()
        }}
      />

      <TransferirServicioOld
        servicioId={servicioIdActive}
        show={showTransferirOld}
        onHide={() => setShowTransferirOld(false)}
        onUpdate={() => {
          setShowTransferirOld(false);
          fetchData()
        }}
      />

      <Card className="rounded">
        <Card.Header className="bg-transparent px-4">
          <div className="d-flex justify-content-between align-items-center">
            <div className="me-4 d-flex align-items-center">
              <Button
                ref={target}
                variant="outline-dark"
                onClick={() => setShow(!show)}
                data-tip="Ocultar/Mostrar Columnas"
              >
                <span className="material-icons mt-1">grid_on</span>
              </Button>

              <Overlay target={target.current} show={show} placement="bottom">
                {(props) => (
                  <Popover id="popover-basic" {...props}>
                    <Popover.Header as="h3">Columnas visibles</Popover.Header>
                    <Popover.Body>
                      {table.allColumns.map((column) => (
                        <div key={column.id}>
                          <label>
                            <input
                              type="checkbox"
                              {...column.getToggleHiddenProps()}
                            />{" "}
                            {column.Header}
                          </label>
                        </div>
                      ))}
                    </Popover.Body>
                  </Popover>
                )}
              </Overlay>
              <span className="h4 mx-3">Nombre: {data.user.nombre}</span>
            </div>
            <div className="d-flex gap-2">
              <Button
                variant="outline-dark"
                onClick={() =>
                  navigate("/usuarios", {
                    state: {
                      lastPage: location.state?.lastPage,
                      query: location.state?.query,
                      back: location.state?.back,
                    },
                  })
                }
                data-tip="Volver atras"
              >
                <IoMdArrowRoundBack />
              </Button>
            </div>
          </div>
        </Card.Header>
        <Card.Body>
          <Table table={table} loading={loading} />
        </Card.Body>
        <Card.Footer className="">
          <Pagination
            total={data.total}
            setCurrentPage={setCurrentPage}
            page={page}
            pageSize={data.pageSize}
          />
        </Card.Footer>
      </Card>
    </Layout>
  );
};

export default Servicios;
