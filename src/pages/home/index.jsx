
import React, { useEffect, useState, useRef } from "react";
import Layout from "../../layouts/Layout";
import "../../App.css";
import {
  Card,
  Button,
  Col,
  Row,
  Modal,
  Popover,
  Overlay,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  Badge,
  InputGroup,
  ModalHeader,
  OverlayTrigger,
  Tooltip
} from "react-bootstrap";
import ReactTooltip from "react-tooltip";
import Table from "../../components/Table";
import { useTable } from "react-table";
import { useDebounce } from "../../hooks";
import { useForm, Controller } from "react-hook-form";
import PhoneInput, { isValidPhoneNumber } from 'react-phone-number-input'
import { Link, useNavigate } from "react-router-dom";
import { BsThreeDotsVertical } from 'react-icons/bs'
import { FaInfoCircle } from "react-icons/fa";
import { MdInfo } from 'react-icons/md';
import { RiDeleteBin6Fill } from 'react-icons/ri';

const tooltipMsg2 = `La llave ("community string") es una contraseña utilizada en SNMP` +
  `para autenticar el acceso a los dispositivos de red y permite a los administradores` +
  `de red acceder y controlar los dispositivos de manera segura.`
const tooltipMsg3 = "Telnet le permitirá el control remoto de los equipos" +
  "remotos por medio de entradas y salidas basadas en texto";
const tooltipMsg4 = "El modelo y la marca de un dispositivo permiten a los administradores" +
  "verificar la compatibilida y la capacidad de dicho dispositivo dentro de la red";



const ModalOlt = ({ onHide, onUpdate, show, row = null}) => {
  const [data, setData] = useState([]);


  const {
    control,
    register,
    handleSubmit,
    watch,

    formState: { errors },
    reset,
    setValue,
  } = useForm();



  const isAddMode = !row;

  const onHidden = () => {
    setData([]);
    onHide();
  };


  useEffect(() => {
    if (!isAddMode) {
      reset(row)

    }
  }, [row])


  return (
    <Modal size="xl" show={show} onHide={onHidden}  className="modal-dialog-scrollable font-color-modal modal-content-olt "  >
      <Modal.Header className="modal-v" closeButton>
        <Modal.Title> <strong>{row?.name}</strong>
          <h5 className="font-color-modal" style={{ fontSize: 14 }}><strong className="me-2">IP:</strong>{row?.public_ip}</h5>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="ms-3 me-3 moBody">
        <Row >
          <Col>
            <Card className="card-modal border border-white  " style={{ width: '18rem' }}>
              <Card.Body className="modal-styles ">
                <h3 style={{ paddingBottom: 15, marginLeft: 10, marginTop: -3 }}><strong>Telnet</strong> <OverlayTrigger
                  placement="left"
                  overlay={<Tooltip className="large-tooltip" id="info-tooltip">{tooltipMsg3}</Tooltip>}
                >
                  <Button
                    className="eliminarB customB "
                    variant="light"
                    size="sm"
                    style={{ float: 'right' }}
                  >
                    <MdInfo style={{ fontSize: 35, textAlign: "right", color: "#707070", paddingBottom: 10 }} />
                  </Button>
                </OverlayTrigger></h3>
                <p style={{ marginBottom: -9, marginLeft: 10, color: "#707070" }}><strong className="me-2" >Puerto TCP:</strong>{row?.telnet_port}</p>
                <hr style={{ height: 2, color: "#707070" }} />
                <p style={{ marginTop: -9, marginLeft: 10, color: "#707070", marginBottom: 65 }}><strong className="me-2">Usuario:</strong></p>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card className="card-modal border border-white   " style={{ width: '18rem', borderRadius: 10 }}>
              <Card.Body className="modal-styles mb-5">
                <h3 style={{ paddingBottom: 15, marginLeft: 10, marginBottom: 8 }}><strong>Hardware</strong> <OverlayTrigger
                  placement="left"
                  overlay={<Tooltip className="large-tooltip" id="info-tooltip">{tooltipMsg4}</Tooltip>}
                >
                  <Button
                    className="eliminarB customB "
                    variant="light"
                    size="sm"
                    style={{ float: 'right' }}
                  >
                    <MdInfo style={{ fontSize: 35, textAlign: "right", color: "#707070", paddingBottom: 10 }} />
                  </Button>
                </OverlayTrigger></h3>
                <p style={{ marginBottom: -9, marginLeft: 10, color: "#707070" }}><strong className="me-2">Marca:</strong>{row?.technology}</p>
                <hr style={{ height: 2, color: "#707070" }} />
                <p style={{ marginTop: -9, marginLeft: 10, color: "#707070", marginBottom: 65 }}><strong className="me-2">Modelo:</strong></p>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card className="card-modal border border-white" style={{ width: '28rem', borderRadius: 10 }}>
              <Card.Body className="modal-styles">
                <h3 style={{ paddingBottom: 15, marginLeft: 10, marginBottom: 8 }}><strong>Community String</strong> <OverlayTrigger
                  placement="left"
                  overlay={<Tooltip className="large-tooltip" id="info-tooltip">{tooltipMsg2}</Tooltip>}
                >
                  <Button
                    className="eliminarB customB "
                    variant="light"
                    size="sm"
                    style={{ float: 'right' }}
                  >
                    <MdInfo style={{ fontSize: 35, textAlign: "right", color: "#707070", paddingBottom: 10 }} />
                  </Button>
                </OverlayTrigger></h3>
                <p style={{ marginBottom: -9, marginLeft: 10, color: "#707070" }}><strong className="me-2">Llave de Escritura:</strong></p>
                <hr style={{ height: 2, color: "#707070" }} />
                <p style={{ marginTop: -9, marginLeft: 10, color: "#707070" }}><strong className="me-2">Llave de Lectura:</strong></p>
                <hr style={{ height: 2, color: "#707070" }} />
                <p style={{ marginTop: -9, marginLeft: 10, color: "#707070" }}><strong className="me-2">Puerto UDP:</strong></p>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body >
      <Modal.Footer >
        <Button onClick={onHidden} variant="outline-secondary">
          Cerrar
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

const Olt = () => {
  const navigate = useNavigate();
  const [data, setData] = useState({
    data: [],
    has_prev: false,
    has_next: false,
    pages: 0,
    total: 0,
    pageSize: 0,
  });
  const [selectedColumn, setSelectedColumn] = useState("__all__");
  const target = React.useRef(null);
  const [show, setShow] = useState(false);
  const [page, setCurrentPage] = useState(1);
  const [query, setQuery] = useState("");

  const [showModal, setShowModal] = useState(false);
  const [activeId, setActiveId] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showAccess, setShowAccess] = useState(false);
  const [selectedData, setSelectedData] = useState(null);
  const [olt, setOlt] = useState([]);



  useEffect(() => {
    ReactTooltip.rebuild();
  });


  const columns = React.useMemo(
    () => [
      {
        Header: "ID",
        accessor: "_id",
        show: true,
        width: "auto",
      },
      {
        Header: "Nombre",
        accessor: "name",
        width: "auto",
      },
      {
        Header: "Marca",
        accessor: "technology",
        width: "auto",
      },
      {
        Header: "IP Privada",
        accessor: "private_ip",
        width: "auto",
      },
      {
        Header: "IP Pública",
        accessor: "public_ip",
        width: "auto",
      },
      {
        Header: "Puerto Telnet TCP",
        accessor: "telnet_port",
        width: "auto",
      },
      {
        Header: "Opciones ",
        accessor: "opciones",
        Cell: ({ row }) => (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <button className="eliminarB customB" style={{ fontSize: 20, color: "#4040ff" }}
              onClick={() => {
                setSelectedData(row.original);
                setShowModal(true);
              }}>
              < FaInfoCircle />
            </button>
            <button className="eliminarB customB ms-4 " style={{ fontSize: 20, color: "red" }}>
              < RiDeleteBin6Fill />
            </button>
          </div>
        ),
        width: "auto"
      },
    ],
    []
  );

  useEffect(() => {
    fetch("http://10.37.37.203:8080/api/olts")
      .then((response) => response.json())
      .then((data) => setOlt(data))
      .catch((error) => console.error(error));
  }, []);

  const table = useTable({
    columns,
    data: olt.data || [],
    initialState: {
      hiddenColumns: columns.map((column) => {
        if (column.show === false) return column.accessor || column.id;
      }),
    },
  });



  return (
    <Layout title="Olt's">
      <ReactTooltip effect="solid" place="left" />
      <ModalOlt  row={selectedData}   onHide={() => setShowModal(false)}  onUpdate={() => {
        setShowModal(false);
      }} show={showModal}  />
     <Row className="table-olt" >
      <Col  xs={8} className="ms-5 me-0 "  >
      <Table table={table} data={olt} />
      </Col>
      <Col xs={2}  >
        <Button
          className="eliminarB customB mt-2 mb-2 "
          variant="outline-dark"
          onClick={() => {
            setShowModal(true);
          }}
        >
          <BsThreeDotsVertical style={{ fontSize: 20}} />
        </Button>
      </Col>
      </Row>
    </Layout>
  );
};

export default Olt;
