import React from 'react';
import Layout from "../../layouts/Layout";
import "../../App.css";
import {
  Card,
  Button,
  Popover,
  Overlay,
  Modal,
  Form,
  ListGroup,
  Badge,
  ButtonGroup,
  ListGroupItem,
  Row,
  Col,
  FormControl,
  FormGroup,
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import { useEffect, useState, useRef } from "react";
import { FaSave, FaChevronLeft, FaInfoCircle, FaEye, FaEyeSlash } from "react-icons/fa";
import { useForm, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import CardOlt from "../../components/CardOlt";
import AddModelOnu from "../../components/AddModelOnu";


function prueba() {

  return (
    <>
      <Layout title="Page">
        <Row>
          <Col sm={8}></Col>
          <Col sm={4} >
            <CardOlt />
          </Col>
        </Row>
        <AddModelOnu />
      </Layout>
    </>
  )
}

export default prueba
