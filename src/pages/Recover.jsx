import "../styles/login.scss";
import "../styles/christmas.scss";
import "../styles/new-year.scss";

import logo from "../static/img/logo2.png";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate, Navigate } from "react-router-dom";
import Snowfall from "react-snowfall";
import { IoMdArrowRoundBack } from "react-icons/io";
import { AiOutlineEye } from "react-icons/ai";

import {
  Row,
  Button,
  Col
} from "react-bootstrap";

import { login, BASE_AUTH_URL, useAuth } from "../auth";
import { toast, ToastContainer } from "react-toastify";
import { useEffect } from "react";

import HeartImage from 'src/static/img/heart-992.svg'

const RecoverPage = () => {
  const [see, setSee] = useState('password');
  const [see2, setSee2] = useState('password');
  const { register, handleSubmit, reset, setValue, getValues } = useForm();
  const [toggle, setToggleVerifyCode] = useState(false);
  const [loading, setLoading] = useState(false);
  // const [newYear, setNewYear] = useState(localStorage.getItem("year2023") === null || false)

  let navigate = useNavigate();

  const [logged] = useAuth();

  const onSubmit = (data) => {
    setLoading(true);

    if (!toggle) {
      fetch(`/api/password/request`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then(async (res) => {
          setLoading(false);

          const isJson = res.headers
            .get("content-type")
            ?.includes("application/json");
          const data = isJson ? await res.json() : null;
          if (data == null) {
            toast.info(
              "Su código de recuperación ha sido enviado. Por favor, revise su correo electrónico",
              {
                position: toast.POSITION.TOP_RIGHT,
              }
            );
            setToggleVerifyCode(true);
          } else {
            toast.error(
              "Correo electrónico incorrecto",
              {
                position: toast.POSITION.TOP_RIGHT,
              }
            );
          }
          if (res.ok) return data;
          res.data = data;
          return Promise.reject(res);
        })
        .catch((err) => {
          setLoading(false);
          console.log(err);
          if (err.status == 503) {
            toast.error(
              "360Control está fuera de servicio por mantenimiento. Vuelva pronto 😊",
              {
                position: toast.POSITION.TOP_RIGHT,
              }
            );
          } else {
            toast.error("Error de conexión", {
              position: toast.POSITION.TOP_RIGHT,
            });
          }
        });
    } else {
      fetch(`/api/password/change`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then(async (res) => {
          setLoading(false);
          const isJson = res.headers
            .get("content-type")
            ?.includes("application/json");
          const data = isJson ? await res.json() : null;
          alert('Su contraseña ah sido modificada exitosamente')
          navigate("/login")
          
        })
        .catch((err) => {
          setLoading(false);
          console.log(err.data.detail)

          if (err.data.detail[0].msg == "ensure this value has at least 8 characters") {
            toast.error(
              "Su contraseña debe contener mínimo 8 caracteres",
              {
                position: toast.POSITION.TOP_RIGHT,
              }
            );
          } else if (err.data.detail[0].msg == "passwords do not match") {
            toast.error(
              "Las contraseñas no coinciden",
              {
                position: toast.POSITION.TOP_RIGHT,
              }
            );
          } else if (err.data.detail == "Bad Request") {
            toast.error(
              "Código incorrecto!",
              {
                position: toast.POSITION.TOP_RIGHT,
              }
            );
          } else if (err.status == 503) {
            toast.error(
              "360Control está fuera de servicio por mantenimiento. Vuelva pronto 😊",
              {
                position: toast.POSITION.TOP_RIGHT,
              }
            );
          } else {
            toast.error("Error de conexión", {
              position: toast.POSITION.TOP_RIGHT,
            });
          }
        });
    }








  };


  if (logged) {
    return <Navigate to="/home" replace />;
  }

  return (
    <div className="login-container login-page">
      {/* <Snowfall style={{ zIndex: 100 }} images={images} radius={[30, 1]} /> */}
      {/* <ToastContainer /> */}
      <div className="limiter">
        <section className="wave-wrapper">
          <div
            className="container-login100"
          // style="background-image: url('images/bg-01.jpg');"
          >
            <div
              className="wrap-login100 px-3 px-md-4 py-4 py-md-5"
              style={{ zIndex: "9999" }}
            >

              <div className="px-3 px-md-4 py-1 py-md-2">
                <Row>
                  <Col className="d-flex align-items-center">
                    <Link to="/login" className="login-page">
                      <IoMdArrowRoundBack style={{ fontSize: 30 }} /> Regresar
                    </Link>
                  </Col>
                  <Col>
                    <span
                      className="login100-form-title py-auto d-flex justify-content-end"
                      style={{ position: "relative" }}
                    >
                      <img src={logo} style={{ maxWidth: "40%" }} />
                      {/* <ChristmasTree /> */}
                    </span>
                  </Col>
                </Row>
                <form

                  onSubmit={handleSubmit(onSubmit)}
                  className="login100-form validate-form "
                >

                  {/* <a href="#" className="btn-face m-b-20">
                  <i className="fa fa-facebook-official"></i>
                  Facebook
                </a>
                <a href="#" className="btn-google m-b-20">
                  <img src="images/icons/icon-google.png" alt="GOOGLE" />
                  Google
                </a> */}
                  <div className="mt-4 p-b-9">
                    <span className="txt1">Correo electrónico corporativo</span>
                  </div>
                  <div
                    className="wrap-input100 validate-input"
                    data-validate="Username is required"
                  >
                    <input
                      autocomplete="off"
                      {...register("email")}
                      className="input100"
                      type="text"
                      required
                      disabled={toggle}
                    />
                    <span className="focus-input100"></span>
                  </div>

                  {toggle &&
                    <>
                      <Row>
                        <Col xs>
                          <div className="py-3">
                            <span className="txt1">Nueva contraseña</span>
                          </div>
                          <div
                            className="wrap-input100 validate-input"
                            data-validate="Password is required"
                          >
                            <input
                              autocomplete="off"
                              {...register("password1")}
                              className="input100"
                              type={see}
                              required
                            />
                            <span className="focus-input100"></span>
                          </div>
                        </Col>
                        <Col xs={"auto"} className="d-flex align-items-center ps-0">
                          <Button
                            className="eliminarB pt-4 ps-0"
                            onClick={() => {
                              if (see == 'password') {
                                setSee('text')
                              } else {
                                setSee('password')
                              }

                            }}
                          >
                            <AiOutlineEye className="iconD" data-tip="Mostrar/Ocultar contraseña" style={{ fontSize: 26, marginTop: "30px", color: "#0D6EFD" }} />
                          </Button>
                        </Col>
                      </Row>


                      <Row>
                        <Col xs>
                          <div className="py-3">
                            <span className="txt1">Repita su nueva contraseña</span>
                          </div>
                          <div
                            className="wrap-input100 validate-input"
                            data-validate="Password is required"
                          >
                            <input
                              autocomplete="off"
                              {...register("password2")}
                              className="input100"
                              type={see2}
                              required
                            />
                            <span className="focus-input100"></span>
                          </div>
                        </Col>
                        <Col xs={"auto"} className="d-flex align-items-center ps-0">
                          <Button
                            className="eliminarB pt-4 ps-0"
                            onClick={() => {
                              if (see2 == 'password') {
                                setSee2('text')
                              } else {
                                setSee2('password')
                              }

                            }}
                          >
                            <AiOutlineEye className="iconD" data-tip="Mostrar/Ocultar contraseña" style={{ fontSize: 26, marginTop: "30px", color: "#0D6EFD" }} />
                          </Button>
                        </Col>
                      </Row>

                      <div className="py-3">
                        <span className="txt1">Código recibido</span>
                      </div>
                      <div
                        className="wrap-input100 validate-input"
                        data-validate="Token is required"
                      >
                        <input
                          autocomplete="off"
                          {...register("token")}
                          className="input100"
                          type="text"
                          required
                        />
                        <span className="focus-input100"></span>
                      </div>
                    </>
                  }

                  <div className="container-login100-form-btn mt-4">
                    <button className="login100-form-btn" disabled={loading}>
                      {loading ? "Cargando" : "Ingresar"}
                    </button>
                  </div>
                </form>

              </div>

            </div>
          </div>
          <div className="wave wave1"></div>
          <div className="wave wave2"></div>
          <div className="wave wave3"></div>
          <div className="wave wave4"></div>
        </section>
      </div>
    </div>
  );
};

export default RecoverPage;
