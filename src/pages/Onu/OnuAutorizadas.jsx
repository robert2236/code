import React, { useEffect, useState, useRef, createContext, useContext } from "react";
import Layout from "../../layouts/Layout";
import "../../App.css";
import {
  Card,
  Button,
  Col,
  Row,
  Modal,
  Popover,
  Overlay,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  Badge,
  InputGroup,
  ModalHeader,
  OverlayTrigger,
  Tooltip
} from "react-bootstrap";
import ReactTooltip from "react-tooltip";
import Table from "../../components/Table";
import { useTable } from "react-table";
import { useDebounce } from "../../hooks";
import { useForm, Controller } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import { FaInfoCircle } from "react-icons/fa";
import { RiDeleteBin6Fill } from 'react-icons/ri';
import { AiFillPlusSquare } from 'react-icons/ai';






const ModalOlt = ({ onHide, onUpdate, show, row = null }) => {
  const [data, setData] = useState([]);


  const {
    control,
    register,
    handleSubmit,
    watch,

    formState: { errors },
    reset,
    setValue,
  } = useForm();



  const isAddMode = !row;

  const onHidden = () => {
    setData([]);
    onHide();
  };


  useEffect(() => {
    if (!isAddMode) {
      reset(row)

    }
  }, [row])


  return (
    <Modal size="xl" show={show} onHide={onHidden} className="modal-dialog-scrollable font-color-modal modal-content-olt "  >
      <Modal.Header className="modal-v" closeButton>
        <Modal.Title> <strong>{row?.name}</strong>
          <h5 className="font-color-modal" style={{ fontSize: 14 }}><strong className="me-2">IP:</strong>{row?.public_ip}</h5>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="ms-3 me-3 moBody">
        <Row >
          <Col>
            <Card className="card-modal border border-white  " style={{ width: '18rem' }}>
              <Card.Body className="modal-styles ">
                <h1>Cuerpo Modal</h1>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body >
      <Modal.Footer >
        <Button onClick={onHidden} variant="outline-secondary">
          Cerrar
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

const Olt = () => {
  const navigate = useNavigate();
  const [data, setData] = useState({
    data: [],
    has_prev: false,
    has_next: false,
    pages: 0,
    total: 0,
    pageSize: 0,
  });
  const [selectedColumn, setSelectedColumn] = useState("__all__");
  const target = React.useRef(null);
  const [show, setShow] = useState(false);
  const [page, setCurrentPage] = useState(1);
  const [query, setQuery] = useState("");

  const [showModal, setShowModal] = useState(false);
  const [activeId, setActiveId] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showAccess, setShowAccess] = useState(false);
  const [selectedData, setSelectedData] = useState(null);
  const [onu, setOnu] = useState([]);
  const [olt, setOlt] = useState([]);





  useEffect(() => {
    ReactTooltip.rebuild();
  });
  const columns = React.useMemo(
    () => [
      {
        Header: 'OLT',
        accessor: 'olt_name',
        width: 'auto',
      },
      {
        Header: "Tarjeta",
        accessor: "board",
        width: "auto",
      },
      {
        Header: "Puerto",
        accessor: "port",
        width: "auto",
      },
      {
        Header: "Serial",
        accessor: "sn",
        width: "auto",
      },
      {
        Header: "Tipo",
        accessor: "onu_type",
        width: "auto",
      },
      {
        Header: "Opciones ",
        accessor: "opciones",
       
        Cell: ({ row }) => (
          <button className="eliminarB customB ms-4" style={{ fontSize: 20, color: "red" }}
          onClick={() => {
            const rowData = row.original;
            navigate("/onuForm", { state: { rowData } });
          }}>
            <svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 -960 960 960" width="48"><path fill="red" d="m717-136 142-141-142-142-39 39 75 75H578v55h174l-74 75 39 39Zm1 81q-93 0-158.5-65T494-278q0-92 65.5-157T718-500q92 0 157 65t65 157q0 93-65 158T718-55Zm-352 0-21-134q-16-5-30-13t-27-19l-125 58L47-368l114-83q-1-8-1.5-14.5T159-480q0-8 .5-14.5T161-509L47-593l116-203 127 57q12-10 26-17.5t29-12.5l21-137h228l21 136q16 6 30.5 13t26.5 18l126-57 115 203-63 44q-31-17-64-24t-68-7q-34 0-67 7.5T587-550q-18-28-47-44t-62-16q-55 0-92.5 38T348-480q0 39 22 70.5t57 48.5q-6 19-9.5 38t-3.5 38q0 66 25 125.5T513-55H366Z" /></svg>
          </button>
        ),
        width: "auto"
      },
    ],
    []
  );



  useEffect(() => {
    fetch("http://10.37.37.203:8080/api/onu/unconfigured_onus")
      .then((response) => response.json())
      .then((data) => setOnu(data))

      .catch((error) => console.error(error));
  }, []);



  const table = useTable({
    columns,
    data: onu.data  || [],
    initialState: {
      hiddenColumns: columns.map((column) => {
        if (column.show === false) return column.accessor || column.id;
      }),
    },
  });




  return (
    <Layout title="Onu's Autorizadas">
      <ReactTooltip effect="solid" place="left" />
      <ModalOlt row={selectedData} onHide={() => setShowModal(false)} onUpdate={() => {
        setShowModal(false);
      }} show={showModal} />
      <Row xs="auto" className="mb-3 font-form-input"   >
        <Col className="pe-0">
          <Form.Label  >Filtros</Form.Label>
          <Card style={{ width: '16rem' }} >
            <Card.Body className="m-1">
              <Form.Select className="m-2" style={{ width: '14.5rem' }}>
                <option value="1">Olt</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </Form.Select>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={1} className="mt-4 ps-0">
          <Button
            className="eliminarB customB"
            variant="outline-primary"
            onClick={() => {
              setShowModal(true);
            }}
          >
            <AiFillPlusSquare style={{ height: "100%", width: 60 }} />
          </Button>
        </Col>
      </Row>

        <Table table={table} data={onu} />
      


    </Layout>
  );
};

export default Olt;
