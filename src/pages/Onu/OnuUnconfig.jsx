
import React, { useEffect, useState, useRef } from "react";
import Layout from "../../layouts/Layout";
import "../../App.css";
import {
  Card,
  Button,
  Col,
  Row,
  Modal,
  Popover,
  Overlay,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  Badge,
  InputGroup,
  ModalHeader,
  OverlayTrigger,
  Tooltip
} from "react-bootstrap";
import ReactTooltip from "react-tooltip";
import Table from "../../components/Table";
import { useTable } from "react-table";
import { useDebounce } from "../../hooks";
import { useForm, Controller } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import { FaInfoCircle } from "react-icons/fa";
import { RiDeleteBin6Fill } from 'react-icons/ri';

const ModalOlt = ({ onHide, onUpdate, show, row = null}) => {
  const [data, setData] = useState([]);

 
  const {
    control,
    register,
    handleSubmit,
    watch,

    formState: { errors },
    reset,
    setValue,
  } = useForm();



  const isAddMode = !row;

  const onHidden = () => {
    setData([]);
    onHide();
  };


  useEffect(() => {
    if (!isAddMode) {
      reset(row)

    }
  }, [row])


  return (
    <Modal size="xl" show={show} onHide={onHidden}  className="modal-dialog-scrollable font-color-modal modal-content-olt "  >
      <Modal.Header className="modal-v" closeButton>
        <Modal.Title> <strong>{row?.name}</strong>
          <h5 className="font-color-modal" style={{ fontSize: 14 }}><strong className="me-2">IP:</strong>{row?.public_ip}</h5>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="ms-3 me-3 moBody">
        <Row >
          <Col>
            <Card className="card-modal border border-white  " style={{ width: '18rem' }}>
              <Card.Body className="modal-styles ">
                <h1>Cuerpo Modal</h1>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Modal.Body >
      <Modal.Footer >
        <Button onClick={onHidden} variant="outline-secondary">
          Cerrar
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

const Olt = () => {
  const navigate = useNavigate();
  const [data, setData] = useState({
    data: [],
    has_prev: false,
    has_next: false,
    pages: 0,
    total: 0,
    pageSize: 0,
  });
  const [selectedColumn, setSelectedColumn] = useState("__all__");
  const target = React.useRef(null);
  const [show, setShow] = useState(false);
  const [page, setCurrentPage] = useState(1);
  const [query, setQuery] = useState("");

  const [showModal, setShowModal] = useState(false);
  const [activeId, setActiveId] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showAccess, setShowAccess] = useState(false);
  const [selectedData, setSelectedData] = useState(null);
  const [olt, setOlt] = useState([]);



  useEffect(() => {
    ReactTooltip.rebuild();
  });


  const columns = React.useMemo(
    () => [
      {
        Header: "ID",
        accessor: "_id",
        width: "auto",
      },
      {
        Header: "Marca",
        accessor: "marca",
        width: "auto",
      },
      {
        Header: "Modelo",
        accessor: "technology",
        width: "auto",
      },
      {
        Header: "Wifi SSID's",
        accessor: "ssid",
        width: "auto",
      },
      {
        Header: "Puerto VoIP",
        accessor: "voip",
        width: "auto",
      },
      {
        Header: "Puertos Ethernet",
        accessor: "ethernet",
        width: "auto",
      },
      {
        Header: "Tipo de puerto optico",
        accessor: "optical",
        width: "auto",
      },
      {
        Header: "Modos admitidos",
        accessor: "admitidos",
        width: "auto",
      },
      {
        Header: "Opciones ",
        accessor: "opciones",
        Cell: ({ row }) => (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <button className="eliminarB customB" style={{ fontSize: 20, color: "#4040ff" }}
              onClick={() => {
                setSelectedData(row.original);
                setShowModal(true);
              }}>
              < FaInfoCircle />
            </button>
            <button className="eliminarB customB ms-4 " style={{ fontSize: 20, color: "red" }}>
              < RiDeleteBin6Fill />
            </button>
          </div>
        ),
        width: "auto"
      },
    ],
    []
  );

  /* useEffect(() => {
    fetch("http://10.37.37.97:8080/api/olts")
      .then((response) => response.json())
      .then((data) => setOlt(data))
      .catch((error) => console.error(error));
  }, []);
 */
  const table = useTable({
    columns,
    data: olt.data || [],
    initialState: {
      hiddenColumns: columns.map((column) => {
        if (column.show === false) return column.accessor || column.id;
      }),
    },
  });



  return (
    <Layout title="Onu's No Configuradas">
      <ReactTooltip effect="solid" place="left" />
      <ModalOlt  row={selectedData}   onHide={() => setShowModal(false)}  onUpdate={() => {
        setShowModal(false);
      }} show={showModal}  />
      <Table table={table} data={olt} />
      
     
     
    </Layout>
  );
};

export default Olt;
