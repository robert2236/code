import React, { useContext } from 'react';
import Layout from "../../layouts/Layout";
import "../../App.css";
import {
  Card,
  Button,
  Popover,
  Overlay,
  Modal,
  Form,
  ListGroup,
  Badge,
  ButtonGroup,
  ListGroupItem,
  Row,
  Col,
  FormControl,
  FormGroup,
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import { useEffect, useState, useRef } from "react";
import { FaSave, FaChevronLeft, FaCog } from "react-icons/fa";
import { useForm, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import { useLocation } from "react-router-dom";






function OnuForm(onUpdate) {


  const [options, setOptions] = useState([]);
  const [speedProfiles, setSpeedProfiles] = useState([]);
  const [olt, setOlt] = useState([]);

  const location = useLocation();
  const rowData = location.state?.rowData;

  console.log(rowData)




  useEffect(() => {
    fetch("http://10.37.37.203:8080/api/onu/unconfigured_onus")
      .then(response => response.json())
      .then(data => {
        const onus = data.data;
        setOptions(onus);
      })
      .catch(error => {
        console.error(error);
      });

    fetch("http://10.37.37.203:8080/api/speed_profiles")
      .then(response => response.json())
      .then(data => {
        console.log(data)
        const profiles = data.data;
        setSpeedProfiles(profiles);
      })
      .catch(error => {
        console.error(error);
      });

    
  }, []);


  const { register, handleSubmit, control,  reset, setValue, getValues, formState: { errors } } = useForm();


  const onSubmit = (data) => {
    const vlanNumb = parseFloat(data.vlan);
    const portNumb = parseFloat(data.port)
    const boardNumb = parseFloat(data.board);

    data.vlan = vlanNumb;
    data.port = portNumb;
    data.board = boardNumb;

    
    fetch(`http://10.37.37.203:8080/api/onu/authorize`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => {
        toast.success('La Onu se ha autorizado correctamente.');
        onUpdate();
      })
      .catch((err) => {
        toast.error(`Hubo un error al intentar realizar el registro de la onu`);
      });
  };


  return (
    <>
      <Layout title="ONU's">
        <div style={{ color: "#707070", marginBottom: 15 }}>
          <h1><strong>Autorizar ONU</strong></h1>
        </div>
        <div style={{ color: "#707070", marginBottom: 20 }}>
          <h5>Pre-registre una ONU para ser autorizada automáticamente al detectarse en la OLT</h5>
        </div>
        <Row className='font-form-input '  style={{ display: "flex", justifyContent: "start" }}>
          <Col xs={6} className='onuForm '>
            <Card>
              <Card.Body className='m-4 ' >
                <form onSubmit={handleSubmit(onSubmit)}>
                  <Row className='mb-4 ' >
                    <Col xs={12} md={6}  >
                      <Form.Group   >
                        <Form.Label  >Nombre</Form.Label>
                        <Form.Control {...register("name", { required: true })} />
                      </Form.Group>
                    </Col>
                    <Col xs={12} md={6} >
                      <Form.Group >
                        <Form.Label >Serial</Form.Label>
                        <Controller
                          name="sn"
                          control={control}
                          render={({ field }) => (
                            <Form.Select {...field} style={{backgroundColor: "#F6F6F6"}}>
                              <option >{rowData.sn}</option>
                              {options.map((onu) => (
                                <option key={onu._id} value={onu.sn}>
                                  {onu.sn}
                                </option>
                              ))}
                            </Form.Select>
                          )}
                        />
                        {errors.port && (
                          <Form.Text className="text-danger">
                            {errors.port.message}
                          </Form.Text>
                        )}
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row className='mb-4' >
                    <Col xs={12} md={6}>
                      <Form.Group   >
                        <Form.Label  >Modelo</Form.Label>
                        {<Form.Select {...register("onu_type", { required: true })}  >
                          <option></option>
                          <option value="ZTE-F612">ZTE-F612</option>
                          <option value="2">modelo2</option>
                        </Form.Select>}
                      </Form.Group>
                    </Col>
                    <Col xs={12} md={6}>
                      <Form.Group>
                        <Form.Label>VLAN</Form.Label>
                        <Form.Control
                          type="number"
                          {...register("vlan", {
                            required: "Ingrese un número de VLAN",
                            min: { value: 1, message: "El número de VLAN debe estar entre 1 y 4094" },
                            max: { value: 4094, message: "El número de VLAN debe estar entre 1 y 4094" },
                          })}
                        />
                        {errors.vlan && (
                          <Form.Text className="text-danger">{errors.vlan.message}</Form.Text>
                        )}
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row className='mb-4'>
                    <Col xs={12} md={6}>
                      <Form.Group>
                        <Form.Label>Puerto</Form.Label>
                        <Controller
                          name="port"
                          control={control}
                          render={({ field }) => (
                            <Form.Select {...field} style={{backgroundColor: "#F6F6F6"}}>
                              <option >{rowData.port}</option>
                              {options.map((onu) => (
                                <option key={onu._id} value={onu.port}>
                                  {onu.port}
                                </option>
                              ))}
                            </Form.Select>
                          )}
                        />
                        {errors.port && (
                          <Form.Text className="text-danger">
                            {errors.port.message}
                          </Form.Text>
                        )}
                      </Form.Group>
                    </Col>
                    <Col xs={12} md={6}>
                      <Form.Group>
                        <Form.Label>Tarjeta</Form.Label>
                        <Controller
                          name="board"
                          control={control}
                          render={({ field }) => (
                            <Form.Select {...field} style={{backgroundColor: "#F6F6F6"}}>
                              <option >{rowData.board}</option>
                              {options.map((onu) => (
                                <option key={onu._id} value={onu.board}>
                                  {onu.board}
                                </option>
                              ))}
                            </Form.Select>
                          )}
                        />
                        {errors.board && (
                          <Form.Text className="text-danger">
                            {errors.board.message}
                          </Form.Text>
                        )}
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row className='mb-4'>
                    <Col xs={6}>
                      <Form.Group>
                        <Form.Label>Método de Funcionamiento</Form.Label>
                        <Controller
                          name="onu_mode"
                          control={control}
                          rules={{ required: "Por favor selecciona un metodo" }}
                          render={({ field }) => (
                            <Form.Select {...field}>
                              <option></option>
                              <option value="bridging">bridging</option>
                              <option value="routing">Routing</option>
                            </Form.Select>
                          )}
                        />
                        {errors.onu_mode && (
                          <Form.Text className="text-danger">
                            {errors.onu_mode.message}
                          </Form.Text>
                        )}
                      </Form.Group>
                    </Col>
                    <Col xs={6}>
                    <Form.Group>
                        <Form.Label>OLT</Form.Label>
                        <Controller
                          name="olt_name"
                          control={control}
                          render={({ field }) => (
                            <Form.Select {...field} style={{backgroundColor: "#F6F6F6"}}>
                              <option >{rowData.olt_name}</option>
                              {options.map((onu) => (
                                <option key={onu._id} value={onu.olt_name}>
                                  {onu.olt_name}
                                </option>
                              ))}
                            </Form.Select>
                          )}
                        />
                        {errors.board && (
                          <Form.Text className="text-danger">
                            {errors.board.message}
                          </Form.Text>
                        )}
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row className='mb-4'>
                  <Col xs={12} md={6}>
                      <Form.Group>
                        <Form.Label>Velocidad de Descarga</Form.Label>
                        <Controller
                          name="download_speed_profile"
                          control={control}
                          render={({ field }) => (
                            <Form.Select {...field} style={{backgroundColor: "#F6F6F6"}}>
                              {speedProfiles.map((profile) => (
                                <option key={profile._id} value={profile.name}>
                                  {profile.name}
                                </option>
                              ))}
                            </Form.Select>
                          )}
                        />
                        {errors.upload_speed_profile && (
                          <Form.Text className="text-danger">
                            {errors.upload_speed_profile.message}
                          </Form.Text>
                        )}
                      </Form.Group>
                    </Col>
                    <Col xs={12} md={6}>
                      <Form.Group>
                        <Form.Label>Velocidad de Subida</Form.Label>
                        <Controller
                          name="upload_speed_profile"
                          control={control}
                          render={({ field }) => (
                            <Form.Select {...field} style={{backgroundColor: "#F6F6F6"}}>
                              {speedProfiles.map((profile) => (
                                <option key={profile._id} value={profile.name}>
                                  {profile.name}
                                </option>
                              ))}
                            </Form.Select>
                          )}
                        />
                        {errors.upload_speed_profile && (
                          <Form.Text className="text-danger">
                            {errors.upload_speed_profile.message}
                          </Form.Text>
                        )}
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row >
                    <Col xs={12}>
                      <Form.Label>Comentario</Form.Label>
                      <Form.Control  {...register("address_or_comment", { required: true })} />
                    </Col>
                  </Row>
                  <Row className='mb-4'>
                    <Col xs={12} className='align-right'>
                      <Button className='me-4 btn-olt ' variant="outline-secondary" size="md" style={{ borderRadius: 5, marginTop: 30 }}>
                        <FaChevronLeft style={{ fontSize: 20, marginBottom: '2px', marginRight: '5px' }} /> Volver
                      </Button>
                      <Button type="submit" className=' btn-olt' variant="primary" size="md" style={{ borderRadius: 5, marginTop: 30 }} >
                        <FaSave style={{ fontSize: 20, marginBottom: '2px', marginRight: '5px' }} /> Guardar
                      </Button>{' '}
                    </Col>
                  </Row>
                </form>
              </Card.Body>
            </Card>
          </Col>
          {/*   Card Right */}
          <Col xs={6} className='onuForm'>
            <Card >
              <Card.Body className='m-4' >
                <Row>
                  <Col xs={12}>
                    <Card className='mt-4 mb-4' style={{ border: '2px solid 	#1E90FF' }}>
                      <p className='p-tag'>Especificaciones del Modelo</p>
                      <Card.Body className='m-5'>
                        <Card.Img className="mx-auto d-block onuImg " variant="center" src="https://i.ibb.co/GPf2qPH/vsol.png" width={330} height={220} />
                        <Row style={{ display: "flex", alignItems: "center", height: "50px" }}>
                          <Col xs={6}><hr style={{ width: "40%", border: "1px solid #1E90FF", margin: 0 }} />
                          </Col>
                          <Col className='align-right' xs={6}><button className="eliminarB customB mb-2"> <FaCog style={{ fontSize: 25, color: "#1E90FF" }} /></button></Col>
                        </Row>
                        <Row height={200} >
                          <Col xs={12}>
                            <ul className='mb-3' style={{ listStyleType: "none", padding: "0" }}>
                              <li><strong>Marca</strong>  VSOL</li>
                              <li><strong>Modelo</strong>  V2601RD</li>
                              <li><strong>Wifi SSID's</strong>  0</li>
                              <li><strong>Puertos VoIP</strong>  1</li>
                              <li><strong>Puertos Ethernet</strong>  0</li>
                              <li><strong>Tipo de puerto optico</strong>  UPC</li>
                              <li><strong>Modelos Admitidos</strong>  Bridging/Routing</li>
                            </ul>
                          </Col>
                        </Row>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Layout>
    </>
  )
}

export default OnuForm
