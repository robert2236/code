import React, { useEffect, useState, useRef, useMemo } from "react";
import Layout from "../../layouts/Layout";
import "../../App.css";
import {
  Card,
  Button,
  Col,
  Row,
  Modal,
  Popover,
  Overlay,
  FormControl,
  Form,
  FormGroup,
  ButtonGroup,
  Badge,
  InputGroup,
  ModalHeader,
  OverlayTrigger,
  Tooltip,
  Container,
} from "react-bootstrap";
import ReactTooltip from "react-tooltip";
import Table from "../../components/Table";
import { useTable } from "react-table";
import { useDebounce } from "../../hooks";
import { useForm, Controller } from "react-hook-form";
import PhoneInput, { isValidPhoneNumber } from 'react-phone-number-input'
import { Link, useNavigate } from "react-router-dom";
import { BsThreeDotsVertical } from 'react-icons/bs'
import { FaInfoCircle } from "react-icons/fa";
import { MdInfo, MdOutlineUpload, MdOutlineDownload, MdLockOutline, MdLockOpen, MdAddCircle } from 'react-icons/md';
import { FiDelete } from 'react-icons/fi'
import { FaEye, FaEyeSlash, } from "react-icons/fa";
import { AiFillPlusSquare, AiOutlineEye, AiFillEye } from 'react-icons/ai'
import { Checkbox } from "@material-ui/core";
import { FaSave } from "react-icons/fa";



const ModalVelocidad = ({ onHide, onUpdate, show, row = null }) => {
  const [data, setData] = useState([]);


  const {
    control,
    register,
    handleSubmit,
    watch,

    formState: { errors },
    reset,
    setValue,
  } = useForm();



  const isAddMode = !row;

  const onHidden = () => {
    setData([]);
    onHide();
  };


  useEffect(() => {
    if (!isAddMode) {
      reset(row)

    }
  }, [row])


  return (
    <Modal size="lg" show={show} onHide={onHidden} className="modal-dialog-scrollable font-color-modal modal-size " style={{ marginTop: 220 }} >
      <Modal.Header className="modal-v" closeButton>
        <Modal.Title> <strong>OLT - Cagua</strong>
          <h5 className="font-color-modal" style={{ fontSize: 14 }}></h5>
        </Modal.Title>
      </Modal.Header >
      <Modal.Body className="ms-4 me-4 ">
        <form action="">
          <Row className="mb-4 ">
            <Form.Label className="px-0 mb-0 font-color">Nombre</Form.Label>
            <Form.Control
              type="text"
              id=""
            ></Form.Control>
          </Row>
          <Row className="mb-4">
            <Form.Label className="px-0 mb-0 font-color">Velocidad (kbps)</Form.Label>
            <Form.Control
              type="text"
              id=""
            ></Form.Control>
          </Row>
          <Row>
            <Form.Label className="px-0 mb-0 font-color">Tipo</Form.Label>
          </Row>
          <Row xs="auto">
            <Col>
              <div style={{ backgroundColor: "#F6F6F6", borderRadius: 4, width: 120 }}>
                <Form.Check className="ms-3 mt-1 mb-1 me-3  "
                  inline
                  type="Checkbox"
                  default
                />Subida
              </div>
            </Col>
            <Col >
              <div style={{ backgroundColor: "#F6F6F6", borderRadius: 4, width: 120 }}>
                <Form.Check className="ms-3 mt-1 mb-1 me-3"
                  inline
                  type="Checkbox"
                />Bajada
              </div>
            </Col>
          </Row>
        </form>
      </Modal.Body >
      <Modal.Footer >
        <Button onClick={onHidden} variant="outline-secondary">Cerrar</Button>
        <Button variant="primary"> <FaSave style={{ fontSize: 20, marginBottom: '2px', marginRight: '5px' }} /> Guardar</Button>
      </Modal.Footer>
    </Modal>
  );
};

function EyeBtn() {

  const [isPasswordHidden, setIsPasswordHidden] = useState(false);

  const togglePasswordVisibility1 = () => {
    setIsPasswordHidden(!isPasswordHidden);
  };




  return (
    <Button style={{ backgroundColor: "#FFFFFF", fontSize: 25 }}
      variant="light"
      className="password-toggle-button ms-1 me-3 border-0"
      onClick={togglePasswordVisibility1}
    >
      {isPasswordHidden ? <AiOutlineEye /> : <AiFillEye />}
    </Button>
  )
}

function Configuracion() {



  const navigate = useNavigate();
  const [data, setData] = useState({
    data: [],
    has_prev: false,
    has_next: false,
    pages: 0,
    total: 0,
    pageSize: 0,
  });
  const [selectedColumn, setSelectedColumn] = useState("__all__");
  const target = React.useRef(null);
  const [show, setShow] = useState(false);
  const [page, setCurrentPage] = useState(1);
  const [query, setQuery] = useState("");

  const [showModal, setShowModal] = useState(false);
  const [activeId, setActiveId] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showAccess, setShowAccess] = useState(false);

  const [speedProfiles, setSpeedProfiles] = useState([]);

  const [selectedType, setSelectedType] = useState("all");




  const columns = useMemo(
    () => [
      {
        Header: "ID",
        accessor: "_id",
        width: "auto",
      },
      {
        Header: "Nombre",
        accessor: "name",
        width: "auto",
      },
      {
        Header: "Velocidad kbps",
        accessor: "speed",
        width: "auto",
      },
      {
        Header: "Velocidad Mbps",
        accessor: (row) => (row.speed / 1000).toFixed(2),
        width: "auto",
      },
      {
        Header: "Tipo",
        accessor: (row) => {
          if (row.type === "download") {
            return "bajada";
          } else if (row.type === "upload") {
            return "subida";
          } else {
            return row.type;
          }
        },
        width: "auto",
      },
      {
        Header: "ONU's Asociadas ",
        accessor: "onu",
        width: "auto",
      },
      {
        Header: "Opciones",
        accessor: "options",
        Cell: ({ row }) => (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <button className="eliminarB customB me-3" style={{ fontSize: 28, color: "#4040ff" }}
              onClick={() => {
                setShowModal(true);
              }}
            >
              < MdAddCircle />
            </button>
            <button className="eliminarB customB " style={{ fontSize: 28 }}>
              {row.original.locked ? <MdLockOutline /> : <MdLockOpen />}
            </button>
          </div>
        ),
        width: "auto",
      },
    ],
    []
  );

  useEffect(() => {
    fetch("http://10.37.37.203:8080/api/speed_profiles")
      .then((response) => response.json())
      .then((data) => setSpeedProfiles(data))
      .catch((error) => console.error(error));
  }, []);



  const filteredSpeedProfiles = useMemo(() => {
    if (selectedType === "all") {
      console.log(speedProfiles.data)
      return speedProfiles.data || []

    } else if (selectedType === "upload") {
      return (speedProfiles.data || []).filter(
        (row) => row.type === "upload"
      );
    } else if (selectedType === "download") {
      return (speedProfiles.data || []).filter(
        (row) => row.type === "download"
      );
    }
  }, [selectedType, speedProfiles.data]);

  const table = useTable({
    columns,
    data: filteredSpeedProfiles,
    initialState: {
      hiddenColumns: columns
        .filter((column) => column.show === false)
        .map((column) => column.accessor || column.id),
    },
  });


  return (
    <Layout title="Perfiles de Velocidad">
      <ReactTooltip effect="solid" place="left" />
      <h5 style={{ marginBottom: -20 }} className="font-color">Filtros</h5>
      <div style={{ display: "flex", justifyContent: "start" }}>
        <Card className="mb-4 mt-4" body style={{ maxWidth: 450, borderRadius: 3 }}>
          <ButtonGroup className="m-2">
            <Button variant="outline-primary" className="px-1 " onClick={() => setSelectedType("upload")}><strong><MdOutlineUpload style={{ fontSize: 20 }} /> Subida</strong> </Button>{' '}
            <Button variant="outline-primary" className="px-1 " onClick={() => setSelectedType("download")}><strong><MdOutlineDownload style={{ fontSize: 20 }} /> Bajada</strong></Button>{' '}
            <Button variant="primary" className="px-1" onClick={() => setSelectedType("all")} style={{ width: 80 }}><strong>Todos</strong></Button>{''}
          </ButtonGroup>
          <EyeBtn />
        </Card>
        <Button
          className="eliminarB customB mb-3 mt-3 ms-2 "
          variant="outline-primary"
          onClick={() => {
            setShowModal(true);
          }}
        >
          <AiFillPlusSquare style={{ height: "100%", width: 60 }} />
        </Button>
        <ModalVelocidad
          row={activeId}
          show={showModal}
          onHide={() => {
            setActiveId(null);
            setShowModal(false);
          }}
          onUpdate={() => {
            setActiveId(null);
            setShowModal(false);
          }}
        />
      </div>

      <Table table={table} data={filteredSpeedProfiles} />

    </Layout>
  );
};

export default Configuracion
