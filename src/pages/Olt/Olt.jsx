import React from 'react';
import Layout from "../../layouts/Layout";
import "../../App.css";
import {
  Card,
  Button,
  Popover,
  Overlay,
  Modal,
  Form,
  ListGroup,
  Badge,
  ButtonGroup,
  ListGroupItem,
  Row,
  Col,
  FormControl,
  FormGroup,
  Tooltip,
  OverlayTrigger
} from "react-bootstrap";
import { useEffect, useState, useRef } from "react";
import { FaSave, FaChevronLeft, FaInfoCircle, FaEye, FaEyeSlash } from "react-icons/fa";
import { useForm, Controller } from "react-hook-form";
import { toast } from "react-toastify";



const tooltipMsg1 = "El nombre de la OLT proporciona una etiqueta descriptiva para identificar de manera única a este equipo en la red" +
  " La dirección IP de la OLT es esencial para acceder a ella a" +
  "través de la red y llevar a cabo tareas de configuración, monitorio y mantenimiento";
const tooltipMsg2 = `La llave ("community string") es una contraseña utilizada en SNMP` +
  `para autenticar el acceso a los dispositivos de red y permite a los administradores` +
  `de red acceder y controlar los dispositivos de manera segura.`
const tooltipMsg3 = "Telnet le permitirá el control remoto de los equios" +
  "remotos por medio de entradas y salidas basadas en texto";
const tooltipMsg4 = "El modelo y la marca de un dispositivo permiten a los administradores" +
  "verificar la compatibilida y la capacidad de dicho dispositivo dentro de la red";




function Olt(onUpdate) {

  const [passwordType, setPasswordType] = useState("password");
  const [isPasswordHidden, setIsPasswordHidden] = useState(true);
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');
  const { register, handleSubmit, reset, setValue, getValues, formState: { errors } } = useForm();


  function handleChange(e) {
    setPassword(e.target.value);
  }

  const togglePasswordVisibility1 = () => {
    setIsPasswordHidden(!isPasswordHidden);

  };




  const onSubmit = (data) => {
    console.log(data);
    data.snmp_enable = true;
    fetch(`http://10.37.37.97:8080/api/olts`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => {
        toast.success('Su activación se ha realizado correctamente.');
        onUpdate();
      })
      .catch((err) => {
        toast.error(`Hubo un error al intentar realizar la activación`);
      });
  };


  return (
    <Layout title="OLT">
      <div style={{ color: "#707070", marginBottom: 15 }}>
        <h1>Agregar</h1>
      </div>
      <div style={{ color: "#707070", marginBottom: 20 }}>
        <h5>Complete el siguiente formulario con los datos requeridos para completar su solicitud</h5>
      </div>
      <Card className='olt' >
        <form onSubmit={handleSubmit(onSubmit)}>
          <Row className='m-5' >
            <Col xs={6} className='pe-5 onuForm'>
              <Row  >
                <Col >
                  <Form.Group as={Col} xs={12}  >
                    <h3 className='font-form-olt' style={{ textAlign: "right" }}>
                      <strong>OLT</strong>
                      <OverlayTrigger
                        placement="left"
                        overlay={<Tooltip className="large-tooltip largeTool" id="info-tooltip">{tooltipMsg1}</Tooltip>}
                      >
                        <Button
                          className="eliminarB customB"
                          variant="light"
                          size="sm"
                        >
                          <FaInfoCircle style={{ fontSize: 25, marginLeft: 5, color: "#CCCCCC" }} />
                        </Button>
                      </OverlayTrigger>
                    </h3>
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Form.Group as={Col} xs={12}  >
                    <Form.Label  >Nombre</Form.Label>
                    <Form.Control  {...register("name", { required: true })} />
                  </Form.Group>
                </Col>
              </Row>
              <Row style={{ marginTop: 35 }}>
                <Col xs={6}  >
                  <Form.Label >IP Pública</Form.Label>
                  <Form.Control
                    {...register("public_ip", {
                      required: "Ip pública es requerida",
                      pattern: {
                        value: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
                        message: "Formato invalido de Ip pública",
                      },
                    })}
                  />
                  {errors.public_ip && <span className='text-danger'>{errors.public_ip.message}</span>}
                </Col>
                <Col xs={6} >
                  <Form.Label >IP Privada</Form.Label>
                  <Form.Control  {...register("private_ip", {
                    required: "Ip privada es requerida",
                    pattern: {
                      value: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
                      message: "Formato invalido de Ip privada",
                    },
                  })} />
                  {errors.public_ip && <span className='text-danger'>{errors.public_ip.message}</span>}
                </Col>
              </Row>
              <Row style={{ marginTop: 25 }} className='onuOlt' >
                <Col xs={6}  >
                  <h3 className='font-form-olt' style={{ textAlign: "right", color: "#707070", marginTop: 30 }}>
                    <strong>Telnet</strong>
                    <OverlayTrigger
                      placement="left"
                      overlay={<Tooltip className="large-tooltip largeTool" id="info-tooltip">{tooltipMsg3}</Tooltip>}
                    >
                      <Button
                        className="eliminarB customB"
                        variant="light"
                        size="sm"
                      >
                        <FaInfoCircle style={{ fontSize: 25, marginLeft: 5, color: "#CCCCCC" }} />
                      </Button>
                    </OverlayTrigger>
                  </h3>
                </Col>
                <Col xs={6}  >
                  <h3 className='font-form-olt' style={{ textAlign: "right", color: "#707070", marginTop: 30 }}>
                    <strong>SSH</strong>
                    <OverlayTrigger
                      placement="left"
                      overlay={<Tooltip className="large-tooltip largeTool" id="info-tooltip">{tooltipMsg4}</Tooltip>}
                    >
                      <Button
                        className="eliminarB customB"
                        variant="light"
                        size="sm"
                      >
                        <FaInfoCircle style={{ fontSize: 25, marginLeft: 5, color: "#CCCCCC" }} />
                      </Button>
                    </OverlayTrigger>
                  </h3>
                </Col>
              </Row>
              <Row style={{ marginTop: -7 }}>
                <Col xs={6} >
                  <Form.Label >Puerto</Form.Label>
                  <Form.Control
                    {...register("telnet_port", {
                      required: "Telnet port is required",
                      maxLength: {
                        value: 4,
                        message: "El puerto Telnet debe tener como máximo 4 dígitos",
                      },
                      pattern: {
                        value: /^\d+$/,
                        message: "El puerto Telnet solo debe contener caracteres numéricos",
                      },
                    })}
                  />
                  {errors.telnet_port && <span className='text-danger'>{errors.telnet_port.message}</span>}
                </Col>
                <Col xs={6}>
                  <Form.Label >Puerto</Form.Label>
                  <Form.Control
                    {...register("ssh_port", {
                      required: "SSH port is required",
                      maxLength: {
                        value: 6,
                        message: "El puerto SSH debe tener como máximo 6 dígitos",
                      },
                      pattern: {
                        value: /^\d+$/,
                        message: "El puerto SSH solo debe contener caracteres numéricos",
                      },
                    })}
                  />
                  {errors.ssh_port && <span className='text-danger'>{errors.ssh_port.message}</span>}
                </Col>
              </Row>
              <Row style={{ marginTop: 35 }}>
                <Col xs={6}>
                  <Form.Label>Usuario</Form.Label>
                  <Form.Control
                    {...register("telnet_username", {
                      required: "Ingrese un nombre de usuario Telnet válido",
                      pattern: {
                        value: /^[a-z0-9]+$/i,
                        message: "Solo se permiten caracteres alfanuméricos",
                      },
                    })}
                  />
                  {errors.telnet_username && <span className='text-danger'>{errors.telnet_username.message}</span>}
                </Col>
                <Col xs={6}>
                  <Form.Label>Usuario</Form.Label>
                  <Form.Control
                    {...register("ssh_username", {
                      required: "Ingrese un nombre de usuario SSH válido",
                      pattern: {
                        value: /^[a-z0-9]+$/i,
                        message: "Solo se permiten caracteres alfanuméricos",
                      },
                    })}
                  />
                  {errors.ssh_username && <span className='text-danger'>{errors.ssh_username.message}</span>}
                </Col>
              </Row>
              <Row style={{ marginTop: 25 }}>
                <Col xs={6}>
                  <Form.Label>Contraseña</Form.Label>
                  <Form.Control
                    {...register("telnet_password", {
                      required: "Ingrese una contraseña",
                      minLength: {
                        value: 8,
                        message: "La contraseña debe tener al menos 8 caracteres",
                      },
                    })}
                  />
                  {errors.telnet_password && <span className='text-danger'>{errors.telnet_password.message}</span>}
                </Col>
                <Col xs={6}>
                  <Form.Label>Contraseña</Form.Label>
                  <Form.Control
                    {...register("ssh_password", {
                      required: "Ingrese una contraseña",
                      minLength: {
                        value: 8,
                        message: "La contraseña debe tener al menos 8 caracteres",
                      },
                    })}
                  />
                  {errors.ssh_password && <span className='text-danger'>{errors.ssh_password.message}</span>}
                </Col>
              </Row>
              <Row style={{ marginTop: 35 }}>
                <Col xs={12}>
                  <Form.Label>Método de Conexión</Form.Label>
                  <Form.Select {...register("connection", { required: "Por favor seleccione un método de conexión" })}>
                    <option></option>
                    <option value="telnet">telnet</option>
                    <option value="ssh">ssh</option>
                  </Form.Select>
                  {errors.connection && <span className='text-danger'>{errors.connection.message}</span>}
                </Col>
              </Row>
            </Col>
            {/* Card Number 2 */}
            <Col xs={6} className='onuForm' >
              <Row>
                <Col >
                  <Form.Group as={Col} xs={12} >
                    <h3 className='font-form-olt' style={{ textAlign: "right" }}>
                      <strong>Community String</strong>
                      <OverlayTrigger
                        placement="left"
                        overlay={<Tooltip className="large-tooltip largeTool" id="info-tooltip">{tooltipMsg2}</Tooltip>}
                      >
                        <Button
                          className="eliminarB customB"
                          variant="light"
                          size="sm"
                        >
                          <FaInfoCircle style={{ fontSize: 25, marginLeft: 5, color: "#CCCCCC" }} />
                        </Button>
                      </OverlayTrigger>
                    </h3>
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col >
                  <Form.Group as={Col} xs={12} >
                    <Form.Label >Llave de Lectura ("Comunity String"){" "}</Form.Label>
                    <Form.Control   {...register("snmp_read", { required: true })} />
                    <p className='font-color'>Generada automáticamente por la OLT</p>
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <Form.Label >Llave de Escritura ("Comunity String"){" "}</Form.Label>
                  <Form.Control   {...register("snmp_write", { required: true })} />
                  <p className='font-color'>Generada automáticamente por la OLT</p>
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <Form.Label >Puerto UDP{" "}</Form.Label>
                  <Form.Control  {...register("snmp_port", {
                    required: "Puerto UDP es requerido",
                    maxLength: {
                      value: 6,
                      message: "El puerto UDP debe tener como máximo 6 dígitos",
                    },
                    pattern: {
                      value: /^\d+$/,
                      message: "El puerto UDP solo debe contener caracteres numéricos",
                    },
                  })} />
                </Col>
              </Row>
              <Row style={{ marginTop: 35 }}>
                <Col>
                  <Form.Group as={Col} xs={12}  >
                    <h3 className='font-form-olt' style={{ textAlign: "right" }}>
                      <strong>Hardware</strong>
                      <OverlayTrigger
                        placement="left"
                        overlay={<Tooltip className="large-tooltip largeTool" id="info-tooltip">{tooltipMsg4}</Tooltip>}
                      >
                        <Button
                          className="eliminarB customB"
                          variant="light"
                          size="sm"
                        >
                          <FaInfoCircle style={{ fontSize: 25, marginLeft: 5, color: "#CCCCCC" }} />
                        </Button>
                      </OverlayTrigger>
                    </h3>
                  </Form.Group>
                </Col>
              </Row>
              <Row style={{ marginTop: 15 }}>
                <Col xs={12}>
                  <Form.Label>Marca</Form.Label>
                  <Form.Select {...register("technology", { required: "Por favor seleccione una marca" })}>
                    <option></option>
                    <option value="zte">zte</option>
                    <option value="vsol">vsol</option>
                  </Form.Select>
                  {errors.technology && <span className='text-danger'>{errors.technology.message}</span>}
                </Col>
              </Row>
              <Row style={{ marginTop: 25 }}>
                <Col xs={12}>
                  <Form.Label>Modelo</Form.Label>
                  <Form.Control />
                </Col>
              </Row>
              <Row>
                <Col xs={12} className='align-right'>
                  <Button className='me-4 btn-olt ' variant="outline-secondary" size="md" style={{ borderRadius: 5, marginTop: 30 }}>
                    <FaChevronLeft style={{ fontSize: 20, marginBottom: '2px', marginRight: '5px' }} /> Volver
                  </Button>
                  <Button type="submit" className=' btn-olt' variant="primary" size="md" style={{ borderRadius: 5, marginTop: 30 }} >
                    <FaSave style={{ fontSize: 20, marginBottom: '2px', marginRight: '5px' }} /> Guardar
                  </Button>{' '}
                </Col>
              </Row>
            </Col>
          </Row>
        </form>
      </Card>


    </Layout>
  )
}


export default Olt
