import React, { useState, useEffect, useContext } from 'react';
import Layout from "./../layouts/Layout";
import Pagination from "./../components/Pagination";
import ReactTooltip from "react-tooltip";
import { Button, Form, Card, Row, Col, Popover, OverlayTrigger } from 'react-bootstrap';
import { IoMdNotifications } from "react-icons/io";
import { HiArchive } from "react-icons/hi";
import { MdOutlineAccessTimeFilled } from "react-icons/md";
import { AiFillWarning, AiFillSafetyCertificate, AiFillExclamationCircle, AiFillAlert, AiFillEye } from "react-icons/ai";
import { http } from "../auth";
import { toast } from "react-toastify";
import Contador from "./../components/Contador";
import { useForm, Controller } from "react-hook-form";
import Select from "react-select";
import { useTable } from "react-table";
import { useDebounce } from "./../hooks";
import Table from "../components/TableNotify";

const Notificaciones = () => {
  const [page, setCurrentPage] = useState(1);
  const [filter, setFilter] = useState("todos");
  const [loading, setLoading] = useState(false);
  const [archivados, setArchivados] = useState(false);
  const [notifications, setNotifications] = useState({ data: [] });
  const [query, setQuery] = useState("");
  const debouncedSearchTerm = useDebounce(query, 500);

  const {
    control,
    register,
    handleSubmit,
    watch,
    reset,
  } = useForm();

  useEffect(() => {
    ReactTooltip.rebuild();
  });

  const options = React.useMemo(
    () => [
      { label: "Todos", value: "todos" },
      { label: "Sin prioridad", value: "default" },
      { label: "Información", value: "info" },
      { label: "Error", value: "error" },
      { label: "Alerta", value: "warning" },
      { label: "Confirmación", value: "success" },
    ],
    []
  );

  const columns = React.useMemo(
    () => [
      {
        Header: "Tipo de notificación",
        accesor: "theme",
        width: "20%",
        Cell: ({ row }) => (
          <div className='ms-5 d-flex align-items-center' style={{ boxSizing: "border-box", height: '5%' }} >
            <span style={{ paddingTop: '10px', paddingBottom: '10px', display: "inline-block" }}>
              {(row?.original?.theme == 'info' || row?.original?.theme == 'default') && <AiFillExclamationCircle style={{ fontSize: 18 }} />}
              {row?.original?.theme == 'error' && <AiFillAlert style={{ fontSize: 18 }} />}
              {row?.original?.theme == 'warning' && <AiFillWarning style={{ fontSize: 18 }} />}
              {row?.original?.theme == 'success' && <AiFillSafetyCertificate style={{ fontSize: 18 }} />}
            </span>
            <span className="ms-2" style={{ paddingTop: '11px', paddingBottom: '10px', display: "inline-block" }}>
              {(row?.original?.theme == 'info' || row?.original?.theme == 'default') && "Notificación"}
              {row?.original?.theme == 'error' && "Error"}
              {row?.original?.theme == 'warning' && "Alerta"}
              {row?.original?.theme == 'success' && "Confirmación"}
            </span>
          </div>
        ),
      },
      {
        Header: "Fecha",
        accessor: "createdAt",
        width: "15%",
        Cell: ({ value }) => {
          return (
            <div className='ms-3 d-flex align-items-center' style={{ boxSizing: "border-box", height: '5%  ' }} >
              <span style={{ paddingTop: '10px', paddingBottom: '10px', display: "inline-block" }}>
                <MdOutlineAccessTimeFilled style={{ fontSize: 18, marginRight: "10px" }} />
              </span>
              <span style={{ paddingTop: '11px', paddingBottom: '10px', display: "inline-block" }}>
                <Contador fecha={new Date(value)} />
              </span>
            </div>
          )
        }
      },
      {
        Header: "Contenido",
        accessor: "content",
        Cell: ({ value }) => {
          return (
            <div className='d-flex align-items-center mx-3' style={{ boxSizing: "border-box", height: '50px' }} >
              <span style={{ paddingTop: '11px', paddingBottom: '10px', display: "inline-block" }}>
                {value.substr(0, 100)}...
              </span>
            </div>
          )
        }
      },
      {
        Header: "Acciones",
        accessor: "_id",
        width: "8%",
        Cell: ({ row }) => {


          const popover = (
            <Popover id="popover-basic">
              <Popover.Body style={{ whiteSpace: "pre-line" }}>
                {row?.original?.content}
              </Popover.Body>
            </Popover>
          );


          return (
            <div className='d-flex align-items-center me-3'>
              <OverlayTrigger placement="left" overlay={popover}>
                <Button className='eliminarB'
                  style={row?.original?.theme == 'warning' ? { color: "#3b3b3b" } : {}}
                >
                  <AiFillEye style={{ fontSize: 20, marginRight: "2px" }} />
                </Button>
              </OverlayTrigger>

              <Button className='eliminarB'
                style={row?.original?.theme == 'warning' ? { color: "#3b3b3b" } : {}}
                disabled={row?.original.readAt}
                data-tip="Archivar (Marcar como leído)"
                onClick={() => {
                  setLoading(true);
                  http
                    .post(`/realtime/notifications/${row?.original._id}/`)
                    .then((res) => {
                      http({
                        url: `/realtime/notifications`,
                        method: "GET"
                      }).then((res) => {
                        setNotifications(res?.data)
                        setLoading(false);
                      })
                        .catch((err) => {
                          toast.error("Error al consultar notificaciones...")
                          setLoading(false);
                        });
                    })
                    .catch(err => {
                      toast.error(
                        "Hubo un problema al archivar..."
                      );
                      setLoading(false);
                    })
                }}
              >
                <HiArchive style={{ fontSize: 20, marginRight: "2px" }} />
              </Button>
            </div>
          )
        }
      }

    ],
    []
  );

  const table = useTable({
    columns,
    data: notifications.data,
    initialState: {
      hiddenColumns: columns.map((column) => {
        if (column.showC === false) return column.accessor || column.id;
      }),
    },
  });

  let cancelToken;

  const search = (url) => {
    if (typeof cancelToken != typeof undefined) {
      cancelToken.abort();
    }

    cancelToken = new AbortController();

    return http.get(url, {
      signal: cancelToken.signal,
    });
  };


  useEffect(() => {
    setCurrentPage(1)
  }, [filter, archivados])

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      http.get('/realtime/notifications', {
        params: {
          ...(archivados && { archived: true }),
          ...(filter != 'todos' && { filter }),
          page
        }
      }).then(({ data }) => {
        setNotifications(data)
        setLoading(false)
      })
    }, 300);

  }, [archivados, filter, page, debouncedSearchTerm])

  return (
    <Layout title="Centro de notificaciones" icon={<IoMdNotifications />} notify={false}>
      <ReactTooltip effect="solid" place='left' />
      <Card className="rounded">
        <Card.Header className="bg-transparent px-4">
          <Row className="d-flex justify-content-center my-2">
            <Col lg>
              <Form.Group>
                <Form.Label>Tipo de Notificación</Form.Label>
                <Controller
                  name="filtros"
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <Select
                      {...field}
                      onChange={(ev) => {
                        setFilter(ev.value);
                      }}
                      options={options}
                      placeholder="Filtrar..."
                      styles={{
                        menuPortal: (base) => ({
                          ...base,
                          zIndex: 9999,
                        }),
                      }}
                      menuPosition="absolute"
                      menuPortalTarget={document.body}
                    />
                  )}
                />
              </Form.Group>
            </Col>
            <Col style={{ paddingTop: "33px", boxSizing: "border-box" }} lg>
              <Form.Group>
                <Form.Check
                  style={{ color: "grey", display: "inline", fontSize: 19 }}
                  className="me-2"
                  type="switch"
                  id="custom-switch"
                  onClick={() => {
                    setArchivados(!archivados)
                  }}
                />
                <Form.Label style={{ fontSize: 20 }}>Notificaciones Archivadas<HiArchive style={{ fontSize: 20, marginLeft: "5px" }} /></Form.Label>
              </Form.Group>
            </Col>
          </Row>
        </Card.Header>
        <Card.Body>
          {loading ?
            <svg
              className="spinner"
              width="65px"
              height="65px"
              viewBox="0 0 66 66"
              xmlns="http://www.w3.org/2000/svg"
              style={{
                margin: "5px auto",
                display: "block",
              }}
            >
              <circle
                className="path"
                fill="none"
                strokeWidth="6"
                strokeLinecap="round"
                cx="33"
                cy="33"
                r="30"
              ></circle>
            </svg>
            :
            <Table table={table} />
          }
        </Card.Body>
        <Card.Footer style={{ paddingTop: '20px' }}>
          <Pagination
            total={notifications.total}
            setCurrentPage={setCurrentPage}
            page={page}
            pageSize={notifications.pageSize}
          />
        </Card.Footer>
      </Card>
    </Layout>
  );
};

export default Notificaciones;
