import React, { useEffect, useState } from "react";
import { Modal, Button, Badge, ButtonGroup } from "react-bootstrap";
import { http } from "../auth";
import { useTable } from "react-table";
import Table from "./Table";
import { toast } from "react-toastify";
import ReactTooltip from "react-tooltip";
import { BsTrashFill, BsPencil } from "react-icons/bs";
import { FiEdit } from "react-icons/fi";

const VerLogCortesModal = (props) => {
  const [data, setData] = useState([]);

  const columns = React.useMemo(
    () => [
      {
        Header: "Comentario",
        accessor: "comentario",
      },
      {
        Header: "Accion",
        accessor: "accion",
        Cell: ({ value }) => value.toUpperCase()
      },
      {
        Header: "Usuario",
        accessor: "usuario",
      },
      {
        Header: "IP",
        accessor: "ip",
      },
      {
        Header: "Fecha",
        accessor: "fecha",
        Cell: ({ value }) => new Date(value).toLocaleDateString('es-VE'),
      },
    ],
    []
  );

  const table = useTable({
    columns,
    data: data,
  });

  const fetchData = async () => {
    try {
      let response = await http.get(`/servicios/${props.servicioId}/historico`);
      setData(response.data);
    } catch (error) {
      toast.error("Ocurrio un error al cargar el log de este servicio");
      onHidden();
    }
  };


  const onHidden = () => {
    props.onHide();
    setData([]);
  };

  useEffect(() => {
    if (props.show) {
      fetchData();
    }
  }, [props.show]);

  useEffect(() => {
    ReactTooltip.rebuild();
  });

  return (
    <Modal size="lg" show={props.show} onHide={onHidden}>
      <Modal.Header closeButton>
        <Modal.Title>Ver log de este servicio</Modal.Title>
      </Modal.Header>
      <ReactTooltip effect="solid" />
      <Modal.Body>
        {data.length ? (
          <Table className="table-sm" table={table} />
        ) : (
          <p className="h5">No hay datos disponibles para mostrar</p>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onHidden} variant="secondary">
          Cerrar
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default VerLogCortesModal;
