import React, { useEffect, useState } from "react";
import {
  Modal,
  Button,
  Badge,
  Row,
  Col,
  InputGroup,
  FormControl,
  Form,
} from "react-bootstrap";
import axios from "axios";
import { http } from "../auth";
import { useTable } from "react-table";
import Table from "./Table";
import { toast } from "react-toastify";
import { useForm, Controller } from "react-hook-form";
import DatePicker from "react-datepicker";
import es from "date-fns/locale/es";
import { AsyncPaginate } from "react-select-async-paginate";

const defaultValues = {
  referencia: "",
  descripcion: "",
  monto: "",
};

const UserFilterModal = (props) => {
  const {
    control,
    register,
    handleSubmit,
    watch,
    formState: { errors },
    reset,
  } = useForm();

  const [data, setData] = useState([]);
  const [factura, setFactura] = useState({});
  const [startDate, setStartDate] = useState(new Date());
  const [loading, setLoading] = useState(false);

  const onHidden = () => {
    setData([]);
    setFactura({});
    props.onHide();
    reset(defaultValues);
  };

  const loadOptions = async (search, loadedOptions, { page }) => {
    const response = await http(`/ciudades/?q=${search}&page=${page}`);
    const responseJSON = response.data;

    const options = responseJSON.map((el) => ({
      value: el.valor,
      label: el.valor,
    }));

    return {
      options: options,
      hasMore: false,
      additional: {
        page: page + 1,
      },
    };
  };

  const loadOptionsUrbanismo = async (search, loadedOptions, { page }) => {
    const response = await http(`/urbanismos/?q=${search}&page=${page}`);
    const responseJSON = response.data;

    const options = responseJSON.data.map((el) => ({
      value: el.valor,
      label: el.valor,
    }));

    return {
      options: options,
      hasMore: responseJSON.has_next,
      additional: {
        page: page + 1,
      },
    };
  };

  useEffect(() => {
    if (props.show) {
    }
  }, [props.show]);

  const onSubmit = (data) => {
    let queryString = Object.keys(data)
      .map((key) =>
        data[key] ? 'filters=' + encodeURIComponent(`${key};eq;${data[key].value}`) : null
      )
      .filter((el) => el != null).join('&');

    props.setFilters(queryString)
    props.setPage(1)
    props.onHide()
  };

  return (
    <Modal size="sm" show={props.show} onHide={onHidden}>
      <Modal.Header closeButton>
        <Modal.Title>Filtrar por</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form id="form-filter" onSubmit={handleSubmit(onSubmit)}>
          <Row>
            <Col xs={12}>
              <Form.Label>Ciudad</Form.Label>

              <Controller
                name="zona_b_ciudad"
                control={control}
                rules={{
                  required: false,
                }}
                render={({ field }) => (
                  <AsyncPaginate
                    loadOptions={loadOptions}
                    value={field.value}
                    isClearable={true}
                    additional={{
                      page: 1,
                    }}
                    onChange={field.onChange}
                  />
                )}
              />
            </Col>
            <Col xs={12}>
              <Form.Label>Urbanismo</Form.Label>
              <Controller
                name="zona_c_urbanismo"
                control={control}
                rules={{
                  required: false,
                }}
                render={({ field }) => (
                  <AsyncPaginate
                    loadOptions={loadOptionsUrbanismo}
                    isClearable={true}
                    additional={{
                      page: 1,
                    }}
                    onChange={field.onChange}
                  />
                )}
              />
            </Col>
          </Row>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button type="submit" form="form-filter" variant="primary">
          Guardar
        </Button>
        <Button onClick={() => {
          props.setFilters("")
          reset({
            'zona_b_ciudad': null,
            'zona_c_urbanismo': null
          })
        }} variant="secondary">
          Reiniciar filtros
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default UserFilterModal;
