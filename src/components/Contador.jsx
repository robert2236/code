
import React, { useState, useEffect, useContext } from 'react';



const Contador = ({ fecha }) => {
    const [contador, setContador] = useState([]);
    const [timeNow, setTimeNow] = useState([]);
  
    const DATE_UNITS = {
      day: 86400,
      hour: 3600,
      minute: 60,
      second: 1
    }
  
    const getSecondsDiff = timestamp => (Date.now() - timestamp) / 1000
    const getUnitAndValueDate = (secondsElapsed) => {
      for (const [unit, secondsInUnit] of Object.entries(DATE_UNITS)) {
        if (secondsElapsed >= secondsInUnit || unit === "second") {
          const value = Math.floor(secondsElapsed / secondsInUnit) * -1
          return { value, unit }
        }
      }
    }
  
    const getTimeAgo = timestamp => {
      const rtf = new Intl.RelativeTimeFormat()
  
      const secondsElapsed = getSecondsDiff(timestamp)
      const { value, unit } = getUnitAndValueDate(secondsElapsed)
      return rtf.format(value, unit)
    }
  
  
    useEffect(() => {    
        setContador(getTimeAgo(fecha))
        setTimeout(() => {
          setTimeNow(new Date())
        }, 1000)
    }, [timeNow]);
  
    return (
  
      <span>{fecha ? contador : <span>Error</span>}</span>
  
    )
  
  };

  export default Contador;