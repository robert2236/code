import React, { useEffect, useState } from "react";
import {
    Card,
    Button,
    Popover,
    Overlay,
    Modal,
    Form,
    ListGroup,
    Badge,
    ButtonGroup,
} from "react-bootstrap";
import { FiEdit } from "react-icons/fi";
import { useForm, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import { http } from "../auth";
import EditModal from "./EditarModal"

const VerDatosModal = (props) => {
    const [editModal, setEditModal] = useState(false);
    const [mode, setMode] = useState(null);


    return (
        <div>
            <EditModal
                show={editModal}
                onHide={() => {
                    setEditModal(false);
                    setMode(null)
                }}
                onUpdate={() => {
                    props?.fetch();
                    props.onHide();
                    setEditModal(false);
                    setMode(null)
                }}
                mode={mode}
                row={props?.row}
            />
            <Modal centered show={props.show} onHide={props.onHide} size="md">
                <Modal.Header closeButton>
                    <Modal.Title>Ver datos del cliente</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ListGroup variant="flush" className="p-4">
                        <ListGroup.Item>
                            <b>Nombres:</b> {props.row?.nombre}{" "}
                            <Button
                                variant="light"
                                data-tip="Editar"
                                size="sm"
                                onClick={() => {
                                    setEditModal(true);
                                    setMode('nombre')
                                }}
                            >
                                <FiEdit />
                            </Button>{" "}
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <b>Cedula:</b> {props.row?.cedula}{" "}
                            <Button
                                variant="light"
                                data-tip="Editar"
                                size="sm"
                                onClick={() => {
                                    setEditModal(true);
                                    setMode('cedula')
                                }}
                            >
                                <FiEdit />
                            </Button>{" "}
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <b>Correo:</b> {props.row?.email}{" "}
                            <Button
                                variant="light"
                                data-tip="Editar"
                                size="sm"
                                onClick={() => {
                                    setEditModal(true);
                                    setMode('email')
                                }}
                            >
                                <FiEdit />
                            </Button>{" "}
                        </ListGroup.Item>
                        <ListGroup.Item><b>Estado:</b> {props.row?.zona_a_estado}</ListGroup.Item>
                        <ListGroup.Item><b>Ciudad:</b> {props.row?.zona_b_ciudad}</ListGroup.Item>
                        <ListGroup.Item><b>Urbanismo:</b> {props.row?.zona_c_urbanismo}</ListGroup.Item>
                        <ListGroup.Item><b>Dirección:</b> {props.row?.direccion_corta}
                            <Button
                                variant="light"
                                data-tip="Editar"
                                size="sm"
                                onClick={() => {
                                    setEditModal(true);
                                    setMode('direccion_corta')
                                }}
                            >
                                <FiEdit />
                            </Button>{" "}
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <b>Telefono 1:</b> {props.row?.telefono1}{" "}
                            <Button
                                variant="light"
                                data-tip="Editar"
                                size="sm"
                                onClick={() => {
                                    setEditModal(true);
                                    setMode('telefono1')
                                }}
                            >
                                <FiEdit />
                            </Button>{" "}
                        </ListGroup.Item>
                        <ListGroup.Item>
                            <b>Telefono 2:</b> {props.row?.telefono2}{" "}
                            <Button
                                variant="light"
                                data-tip="Editar"
                                size="sm"
                                onClick={() => {
                                    setEditModal(true);
                                    setMode('telefono2')
                                }}
                            >
                                <FiEdit />
                            </Button>{" "}
                        </ListGroup.Item>
                        {/*<ListGroup.Item>
                            <b>Género:</b> {props.row.genero == 'M' ? 'Masculino' : props.row.genero == 'F' ? "Femenino" : "Otros"}
                            {" "}
                            <Button
                                variant="light"
                                data-tip="Editar"
                                size="sm"
                                onClick={() => {
                                    setEditModal(true);
                                    setMode('genero')
                                }}
                            >
                                <FiEdit />
                            </Button>{" "}
                            </ListGroup.Item>*/}
                    </ListGroup>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary" onClick={props.onHide}>
                        Cerrar
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default VerDatosModal;
