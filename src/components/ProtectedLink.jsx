const ProtectedLink = ({ scopes, hidden, special, children, user }) => {

    
    if (user?.scopes?.split(" ").some((el) => hidden?.includes(el))) {
        return null;
    }

    if (scopes?.includes("cuadrilla") && user?.scopes?.includes("all")) {
        return null;
    }

    if (scopes?.includes("auditoria") && user?.scopes?.includes("all")) {
        if (special?.includes("auditoria") && user?.scopes?.includes("all")) {
            return children;
        }
        return null;
    }

    if (user?.scopes?.includes("all")) {
        return children;
    }

    if (user?.scopes?.split(" ").some((el) => scopes?.includes(el))) {
        return children;
    }



    return null;
};

export default ProtectedLink;