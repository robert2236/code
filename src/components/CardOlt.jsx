import React from 'react';
import "../App.css";
import {
    Card,
    Button,
    Popover,
    Overlay,
    Modal,
    Form,
    ListGroup,
    Badge,
    ButtonGroup,
    ListGroupItem,
    Row,
    Col,
    FormControl,
    FormGroup,
    Tooltip,
    OverlayTrigger
} from "react-bootstrap";
import { useEffect, useState, useRef } from "react";
import { FaSave, FaChevronLeft, FaInfoCircle, FaEye, FaEyeSlash } from "react-icons/fa";
import { useForm, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import { BiExit } from "react-icons/bi"
import { BsBoxArrowUpRight } from "react-icons/bs"


function CardOlt() {
    return (
        <>
            <Card style={{ width: '22rem' }}>
                <Card.Body className='m-4 '>
                    <Row>
                        <Col xs={8}>
                    <Card.Title className='ms-3 mb-4'>ONU's autorizadas </Card.Title>
                        </Col >
                        <Col  style={{textAlign: "right"}} xs={4} >
                    <button className="eliminarB customB"><BsBoxArrowUpRight style={{ fontSize: 20, color: "#007bff" }} /></button>
                       </Col>
                    </Row>
                    <Row xs="auto" className="justify-content-center text-center mb-4" style={{ color: "#707070" }}>
                        <Col >
                            <Card style={{ width: '5rem' , borderColor: "#8F8F8F"  }}>
                                <Card.Body className='mt-2 mb-2'>
                                    <p className='m-1' >Domingo</p>
                                    <h1>13</h1>
                                </Card.Body>
                                <Card.Footer style={{ fontSize: 10, backgroundColor: "#8F8F8F", color: "white", borderColor: "#8F8F8F", padding: 0 }} >
                                    12 ONU's
                                </Card.Footer>
                            </Card>
                        </Col>
                        <Col> <Card style={{ width: '5rem', borderColor: "#007bff" }}>
                            <Card.Body className='mt-2 mb-2'>
                                <p className='m-1'>Lunes</p>
                                <h1>14</h1>
                            </Card.Body>
                            <Card.Footer style={{ fontSize: 10, backgroundColor: "#007bff", color: "white", borderColor: "#8F8F8F", padding: 0 }}>
                                12 ONU's
                            </Card.Footer>
                        </Card></Col>
                        <Col>
                            <Card style={{ width: '5rem', borderColor: "#8F8F8F"  }}>
                                <Card.Body className='mt-2 mb-2'>
                                    <p className='m-1'>Martes</p>
                                    <h1>15</h1>
                                </Card.Body>
                                <Card.Footer style={{ fontSize: 10, backgroundColor: "#8F8F8F", color: "white", borderColor: "#8F8F8F", padding: 0 }}>
                                    12 ONU's
                                </Card.Footer>
                            </Card>
                        </Col>
                    </Row>
                    <Row xs="auto" className="justify-content-center text-center mt-3" style={{ color: "#707070" }}>
                        <Col>
                            <h1 className='mt-1' style={{ color: "#007bff", fontWeight: "bold" }}>74 <p style={{ fontSize: 18 }}>ONU's </p>   </h1>
                        </Col>
                        <Col style={{ borderLeft: "1px solid #8F8F8F" }} >
                            <p className='mt-2' style={{ textAlign: "justify", fontSize: 14 }}>
                                <strong style={{ color: "#007bff" }}>62</strong> más<br /> comparadas con  <br /> el dia de ayer
                            </p>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        </>
    )
}

export default CardOlt
