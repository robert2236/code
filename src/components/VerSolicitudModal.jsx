import {
    Button,
    Modal,
    ListGroup,
    Row,
    Col
} from "react-bootstrap";


const VerSolicitudModal = ({ show, onHide, row }) => {
          let options = {
            weekday: 'long',
            month: 'long',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            hour12: true
          };
    return (
        <Modal centered show={show} onHide={onHide} size="lg">
            <Modal.Header closeButton>
                <Modal.Title>Datos del cliente</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <ListGroup variant="flush" className="p-2">
                    <ListGroup.Item>
                        <Row>
                            <Col><b>Nombres: </b>{row?.nombres}</Col>
                            <Col><b>Apellidos: </b>{row?.apellidos}</Col>
                        </Row>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Row>
                            <Col><b>Cedula: </b>{row?.cedula}</Col>
                            <Col><b>Correo: </b>{row?.correo}</Col>
                        </Row>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Row>
                            <Col><b>Fecha de Nacimiento: </b>{row?.fecha_nacimiento}</Col>
                            <Col><b>Género: </b>{row?.genero == "F" ? "Femenino" : "Masculino"}</Col>
                        </Row>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Row>
                            <Col><b>Zona: </b>{row?.urbanismo?.codigo_ciudad}</Col>
                            <Col><b>Urbanismo: </b>{row?.urbanismo?.valor}</Col>
                        </Row>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Row>
                            <Col><b>Dirección: </b>{row?.direccion}</Col>
                            <Col><b>Fecha de solicitud: </b>
                                {row?.fecha_de_solicitud
                                    ? new Date(row.fecha_de_solicitud).toLocaleString('es-VE', options)
                                    : ""}</Col>
                        </Row>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Row>
                            <Col><b>Plan: </b>{row?.plan?.nombre}</Col>
                            <Col><b>Promotor/a: </b>{row?.promotora?.nombre_completo}</Col>
                        </Row>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Row>
                            <Col><b>Telefono 1: </b>{row?.telefono1}</Col>
                            <Col><b>Telefono 2: </b>{row?.telefono2}</Col>
                        </Row>
                    </ListGroup.Item>
                    { row?.estado != 'SOLICITUD' && row?.estado != "EN ESPERA" &&
                        <>
                            <ListGroup.Item>
                                <Row>
                                    <Col><b>IPv4: </b>{row?.ipv4 ? row?.ipv4 : "No asignado"}</Col>
                                    <Col><b>Serial: </b>{row?.mac}</Col>
                                </Row>
                            </ListGroup.Item>
                            <ListGroup.Item>
                                <Row>
                                    <Col><b>Cuadrilla: </b>{row?.cuadrilla ? row?.cuadrilla?.nombre : "No posee"}</Col>
                                    <Col><b>Técnico asignado: </b>{row?.tecnico ? row?.tecnico?.nombres + ' ' + row?.tecnico?.apellidos : "No posee"}</Col>
                                </Row>
                            </ListGroup.Item>
                        </>
                    }                   
                        <>
                            <ListGroup.Item>
                                <Row>
                                    <Col><b>Fecha de instalación: </b>{row?.fecha_de_instalacion ? new Date(row?.fecha_de_instalacion)?.toLocaleString("es-VE", options) : "No posee"}</Col>
                                    <Col><b>Suscripción: </b>{row?.suscripcion?.nombre ? row?.suscripcion?.nombre : "No posee"}</Col>
                                </Row>
                            </ListGroup.Item>
                        </>
                </ListGroup>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-secondary" onClick={onHide}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default VerSolicitudModal;