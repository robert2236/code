import React from "react";
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from "react-pro-sidebar";

import { TbBuildingSkyscraper } from "react-icons/tb";
import { Link, useMatch, useResolvedPath } from "react-router-dom";
import { Dropdown, Button } from "react-bootstrap";
import { useAuth, logout } from "../auth";
import { MdOutlineNotificationsNone, MdLogout, MdSell, MdDns, MdAnalytics,MdOutlineAnalytics } from "react-icons/md";
import { HiOutlineArrowNarrowLeft, HiOutlineArrowNarrowRight } from "react-icons/hi";
import {BiUser} from "react-icons/bi";
import {TbServer} from "react-icons/tb";
import { FaUserAlt, FaWrench } from 'react-icons/fa';
import logo from "../static/img/logo5.png";
import {FiHome} from "react-icons/fi";
import {
  AiOutlineTool,
  AiOutlineDeploymentUnit,
  AiFillAlert,
} from "react-icons/ai";
import { http } from "../auth";
import ProtectedLink from "./ProtectedLink";
import { useState } from "react";
import { toast } from "react-toastify";
import {SiVisualstudiocode} from "react-icons/si"


const CustomLink = ({ children, to, ...props }) => {
  let resolved = useResolvedPath(to);
  let match = useMatch({ path: resolved.pathname, end: true });
  return (
    <MenuItem active={match}>
      {children}
      <Link to={to} />
    </MenuItem>
  );
};

const CustomToggle = React.forwardRef(({ children, onClick, state }, ref,) => (
  <a
    href=""
    ref={ref}
    className="d-flex align-items-center text-white text-decoration-none dropdown-toggle"
    id="dropdownUser1"
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    <div
      style={{
        width: 28,
        height: 28,
        background: "lightgreen",
      }}
      className="rounded-circle me-2"
    ><span className="d-flex justify-content-center align-items-center" style={{ paddingTop: "4px" }}>{children?.substr(0, 1)}</span></div>

    <strong>{state ? "" : children}</strong>
  </a>
));

const SidebarMenu = ({ showMenu, setMenu }) => {
  const [logged, user, tokenProvider] = useAuth();
  const [state, setState] = useState(window.innerWidth < 900 ? true : false);

  const downloadFile = () => {
    http
      .get("/exportar-clientes", {
        responseType: "blob",
      })
      .then(function (response) {
        let blob = new Blob([response.data]);
        let link = document.createElement("a");
        link.href = window.URL.createObjectURL(blob);
        link.download = "EstadoClientes.xlsx";
        link.click();
      })
      .catch((err) => { });
  };


  return (
    <ProSidebar collapsed={state}>
      <SidebarHeader >
        <div className="d-flex justify-content-center"
          onClick={() => setState(!state)}
          style={state ?
            {
              padding: "15px 15px 3px 15px",
              textTransform: "uppercase",
              fontWeight: "bold",
              fontSize: "1.2rem",
              letterSpacing: "1px",
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              position: "relative",
              cursor: "pointer"
            }
            :
            {
              padding: "16px 24px 10px 24px",
              textTransform: "uppercase",
              fontWeight: "bold",
              fontSize: "1.2rem",
              letterSpacing: "1px",
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              position: "relative",
              cursor: "pointer"
            }}
        >
          <img
            alt="Logo"
            data-tip
            data-for='link-to'
            src={logo}
            style={state ?
              {
                maxWidth: "100%",
                margin: "0",
              }
              :
              {
                maxWidth: "61%",
                margin: "0 auto",
              }}
          />
        </div>
        <div className="d-flex justify-content-center">
          <Button className="eliminarB" style={{ boxShadow: "none" }} onClick={() => setState(!state)}>

            {!state ? <HiOutlineArrowNarrowLeft style={{ fontSize: 30 }} /> : <HiOutlineArrowNarrowRight style={{ fontSize: 16 }} />}

          </Button>

        </div>


      </SidebarHeader>
      <SidebarContent style={{ overflowY: "auto" }}>
        <Menu iconShape="circle">
            <MenuItem icon={<FiHome />}>
              Inicio
              <Link to="/home" />
            </MenuItem>
            <MenuItem icon={<TbServer style={{ fontSize: "18px" }} />}>
              ONU's por Autorizar
              <Link to="/onuAutorizadas" />
            </MenuItem>
            <MenuItem icon={<TbServer />}>
              OLT'S
              <Link to="/olt" />
            </MenuItem>
            <MenuItem icon={<MdOutlineAnalytics style={{ fontSize: "18px" }} />}>
              Estadísticas
              <Link to="/estadisticas" />
            </MenuItem>
            <MenuItem icon={<FaWrench style={{ fontSize: "18px" }} />}>
              Configuración
              <Link to="/configuracion" />
            </MenuItem>
            <MenuItem icon={<BiUser />}>
              Usuarios
              <Link to="/usuarios" />
            </MenuItem>
            <MenuItem icon={<BiUser />}>
              ONU's
              <Link to="/onuForm" />
            </MenuItem>
            <MenuItem icon={<TbServer style={{ fontSize: "18px" }} />}>
              ONU's No Configuradas
              <Link to="/onuUnconfig" />
            </MenuItem>
            <MenuItem icon={<SiVisualstudiocode style={{ fontSize: "18px" }} />}>
              Prueba
              <Link to="/prueba" />
            </MenuItem>
          </Menu>

       
      </SidebarContent>
      <SidebarFooter>
     {/*    <Menu style={{ padding: "4.5px 0px 4.5px 0px" }}>
          <MenuItem onClick={() => !user?.scopes?.includes('corporativo') && toast.warning('Permisos insuficientes')} icon={<TbBuildingSkyscraper style={{ fontSize: 26 }} />}>
            {user?.scopes?.includes('corporativo') || user?.scopes?.includes('all') ?
              <a onClick={() => {
                const dt = new Date();
                dt.setTime(dt.getTime() + (1 * 1 * 60 * 60 * 1000));
                const expires = "; expires=" + dt.toGMTString();
                document.cookie = "token=" + tokenProvider?.getTokenInternal()?.access_token + expires + '; domain=fibra360.net';
              }
              } href={`http://juridico.fibra360.net/Log`}>
                360NET Jurídico
              </a>
              :
              '360NET Jurídico'
            }
          </MenuItem>
        </Menu> */}
      </SidebarFooter>
      <SidebarFooter>
        <div style={{ padding: "17px 9px 17px 23px" }}>
          <Dropdown>
            <Dropdown.Toggle as={CustomToggle} state={state} id="dropdown-basic">
              {user?.fullname}
            </Dropdown.Toggle>
            <Dropdown.Menu className="fixedZ">
{/*               <Dropdown.Item className='d-flex align-items-center'>
                <MdPerson style={{ fontSize: 18, marginBottom: '2px', marginRight: '5px' }} />Perfil
              </Dropdown.Item> */}
            {/*   <Dropdown.Item as={Link} to="/ver-mi-caja" className='d-flex align-items-center'>
                <MdSell style={{ fontSize: 16, marginBottom: '2px', marginRight: '5px' }} />Ver mi caja
              </Dropdown.Item> */}
{/*               <Dropdown.Item className='d-flex align-items-center'>
                <MdSettings style={{ fontSize: 18, marginBottom: '2px', marginRight: '5px' }} />Configuración
              </Dropdown.Item> */}
              <Dropdown.Item onClick={() => logout()} className='d-flex align-items-center'
              >
                <MdLogout style={{ fontSize: 18, marginBottom: '2px', marginRight: '5px' }} />Cerrar sesión
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </SidebarFooter>
    </ProSidebar>
  );
};

export default SidebarMenu;
