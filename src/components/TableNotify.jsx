import React, { useEffect } from "react";
import { Table } from "react-bootstrap";
import { useTable } from "react-table";

const IndeterminateCheckbox = React.forwardRef(
  ({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);

    return <input type="checkbox" ref={resolvedRef} {...rest} />;
  }
);

const TableWrapper = ({ table, className = "", loading = false }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    allColumns,
    getToggleHideAllColumnsProps,
    state,
  } = table;

  return (
    <div>
      <table
        style={{ marginBottom: 0, overflow: "hidden" }}
        className={`table table-bordered ${className}`}
        {...getTableProps()}
      >
        {/* Apply the table body props */}
        <tbody {...getTableBodyProps()}>
          {loading && (
            <tr>
              <td colSpan="100%">
                <svg
                  className="spinner"
                  width="65px"
                  height="65px"
                  viewBox="0 0 66 66"
                  xmlns="http://www.w3.org/2000/svg"
                  style={{
                    margin: "5px auto",
                    display: "block",
                  }}
                >
                  <circle
                    className="path"
                    fill="none"
                    strokeWidth="6"
                    strokeLinecap="round"
                    cx="33"
                    cy="33"
                    r="30"
                  ></circle>
                </svg>
              </td>
            </tr>
          )}

          {
            // Loop over the table rows
            React.Children.toArray(
              rows.map((row) => {
                // Prepare the row for display
                prepareRow(row);
                let s = {};

                return (
                  // Apply the row props
                  <tr {...row.getRowProps()} className={
                    (row.original?.theme == 'info' || row.original?.theme == 'default') ? "TableNotifyInfo" :
                      row.original?.theme == 'error' ? "TableNotifyError" :
                        row.original?.theme == 'warning' ? "TableNotifyAlert" :
                          row.original?.theme == 'success' ? "TableNotifySuccess" : "TableNotifyInfo"}>
                    {
                      // Loop over the rows cells
                      row.cells.map((cell) => {
                        // Apply the cell props
                        let value = cell.render("Cell");

                        return (
                          <td
                            {...cell.getCellProps({
                              style: {
                                minWidth: cell.column.minWidth,
                                width: cell.column.width,
                                padding: "0px",
                                borderWidth: "0px"
                              },
                            })}
                          >
                            {value}
                          </td>
                        );
                      })
                    }
                  </tr>
                );
              })
            )
          }
        </tbody>
      </table>
    </div>
  );
};

export default TableWrapper;
