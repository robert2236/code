import React from "react";
import { Pagination, Row, Col } from "react-bootstrap";
import { usePagination, DOTS } from "../hooks";

const PaginationWrapper = ({ total, page, setCurrentPage, pageSize }) => {
  const paginationRange = usePagination({
    currentPage: page,
    totalCount: total,
    siblingCount: 1,
    pageSize,
  });

  let lastPage = paginationRange
    ? paginationRange[paginationRange.length - 1]
    : 1;

  const goToPreviousPage = () => {
    if (page > 1) return setCurrentPage((page) => page - 1);
  };
  const goToNextPage = () => {
    if (page < lastPage) return setCurrentPage((page) => page + 1);
  };
  const goToPage = (page) => setCurrentPage(page);

  return (
    <Pagination style={{ justifyContent: "center" }}>
    <Pagination.First onClick={() => goToPage(1)} />
    <Pagination.Prev onClick={goToPreviousPage} />
    {paginationRange?.map((pageNumber) => {
      if (pageNumber === DOTS) {
        return null;
      }
      return (
        <Pagination.Item
          onClick={() => goToPage(pageNumber)}
          active={pageNumber === page}
          key={pageNumber}
        >
          {pageNumber}
        </Pagination.Item>
      );
    })}
    <Pagination.Next onClick={goToNextPage} />
    <Pagination.Last onClick={() => goToPage(lastPage)} />
    <li className="page-item disabled">
      <a className="page-link">Mostrando {pageSize * (page - 1) + 1 } a {pageSize * page} de {total} entradas</a>
    </li>
  </Pagination>

  );
};

export default PaginationWrapper;
