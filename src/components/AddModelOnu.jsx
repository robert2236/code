import React from 'react';
import "../App.css";
import {
    Card,
    Button,
    Popover,
    Overlay,
    Modal,
    Form,
    ListGroup,
    Badge,
    ButtonGroup,
    ListGroupItem,
    Row,
    Col,
    FormControl,
    FormGroup,
    Tooltip,
    OverlayTrigger,
    InputGroup
} from "react-bootstrap";
import { useEffect, useState, useRef } from "react";
import { FaSave, FaChevronLeft, FaInfoCircle, FaEye, FaEyeSlash } from "react-icons/fa";
import { useForm, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import { BiExit } from "react-icons/bi"
import { BsBoxArrowUpRight } from "react-icons/bs"

function AddModelOnu(onUpdate) {

    const [options, setOptions] = useState([]);
    const [speedProfiles, setSpeedProfiles] = useState([]);

    useEffect(() => {
        fetch("http://10.37.37.203:8080/api/onu/unconfigured_onus")
            .then(response => response.json())
            .then(data => {
                const onus = data.data;
                setOptions(onus);
            })
            .catch(error => {
                console.error(error);
            });

        fetch("http://10.37.37.203:8080/api/speed_profiles")
            .then(response => response.json())
            .then(data => {
                console.log(data)
                const profiles = data.data;
                setSpeedProfiles(profiles);
            })
            .catch(error => {
                console.error(error);
            });
    }, []);

    const { register, handleSubmit, control, reset, setValue, getValues, formState: { errors } } = useForm();


    const onSubmit = (data) => {
        const vlanNumb = parseFloat(data.vlan);
        const portNumb = parseFloat(data.port)
        const boardNumb = parseFloat(data.board);

        data.vlan = vlanNumb;
        data.port = portNumb;
        data.board = boardNumb;

        console.log(data);
        fetch(/* `http://10.37.37.203:8080/api/onu/authorize` */ {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        })
            .then((res) => res.json())
            .then((res) => {
                toast.success('La Onu se ha autorizado correctamente.');
                onUpdate();
            })
            .catch((err) => {
                toast.error(`Hubo un error al intentar realizar el registro de la onu`);
            });
    };



    return (
        <>
            <div style={{ color: "#707070", marginBottom: 25 }}>
                <h2><strong>Agregar Modelo Onu</strong></h2>
            </div>
            <Row className='font-form-input'>
                <Col xs={6}>
                    <Card>
                        <Card.Body className='m-5'>
                            <Row className='mb-4 mt-1' >
                                <Col xs={12} md={6}>
                                    <Form.Group   >
                                        <Form.Label  >Nombre</Form.Label>
                                        <Form.Control />
                                    </Form.Group>
                                </Col>
                                <Col xs={12} md={6} >
                                    <Form.Group >
                                        <Form.Label >Serial</Form.Label>
                                        <Form.Control />
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row className='mb-4' >
                                <Col xs={12} md={6}>
                                    <Form.Group   >
                                        <Form.Label  >Modelo</Form.Label>
                                        {<Form.Select   >
                                            <option></option>
                                            <option value="1">modelo1</option>
                                            <option value="2">modelo2</option>
                                        </Form.Select>}
                                    </Form.Group>
                                </Col>
                                <Col xs={12} md={6} >
                                    <Form.Group>
                                        <Form.Label>VLAN</Form.Label>
                                        <Form.Control
                                            type="number"
                                            {...register("vlan", {
                                                required: "Ingrese un número de VLAN",
                                                min: { value: 1, message: "El número de VLAN debe estar entre 1 y 4094" },
                                                max: { value: 4094, message: "El número de VLAN debe estar entre 1 y 4094" },
                                            })}
                                        />
                                        {errors.vlan && (
                                            <Form.Text className="text-danger">{errors.vlan.message}</Form.Text>
                                        )}
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row className='mb-4'>
                                <Col xs={12} md={6}>
                                    <Form.Group>
                                        <Form.Label>Puerto</Form.Label>
                                        <Controller
                                            name="port"
                                            control={control}
                                            rules={{ required: "Seleccion un número de puerto" }}
                                            render={({ field }) => (
                                                <Form.Select {...field}>
                                                    {options.map((onu) => (
                                                        <option key={onu._id} value={onu.port}>
                                                            {onu.port}
                                                        </option>
                                                    ))}
                                                </Form.Select>
                                            )}
                                        />
                                        {errors.port && (
                                            <Form.Text className="text-danger">
                                                {errors.port.message}
                                            </Form.Text>
                                        )}
                                    </Form.Group>
                                </Col>
                                <Col xs={12} md={6} >
                                    <Form.Group>
                                        <Form.Label>Tarjeta</Form.Label>
                                        <Controller
                                            name="board"
                                            control={control}
                                            rules={{ required: "Por favor selecciona un valor" }}
                                            render={({ field }) => (
                                                <Form.Select {...field}>
                                                    {options.map((onu) => (
                                                        <option key={onu._id} value={onu.board}>
                                                            {onu.board}
                                                        </option>
                                                    ))}
                                                </Form.Select>
                                            )}
                                        />
                                        {errors.board && (
                                            <Form.Text className="text-danger">
                                                {errors.board.message}
                                            </Form.Text>
                                        )}
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row className='mb-4'>
                                <Col xs={12}>
                                    <Form.Group>
                                        <Form.Label>Método de Funcionamiento</Form.Label>
                                        <Controller
                                            name="onu_mode"
                                            control={control}
                                            rules={{ required: "Por favor selecciona un metodo" }}
                                            render={({ field }) => (
                                                <Form.Select {...field}>
                                                    <option></option>
                                                    <option value="bridging">bridging</option>
                                                    <option value="routing">Routing</option>
                                                </Form.Select>
                                            )}
                                        />
                                        {errors.onu_mode && (
                                            <Form.Text className="text-danger">
                                                {errors.onu_mode.message}
                                            </Form.Text>
                                        )}
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12}>
                                    <Form.Label>Imagen Referencial</Form.Label>
                                    <InputGroup className="mb-3">
                                        <Form.Control
                                            placeholder="Ningún Archivo Seleccionado"
                                        />
                                        <Button variant="outline-secondary" id="button-addon2">
                                            Seleccionar...
                                        </Button>
                                    </InputGroup>
                                </Col>
                            </Row>
                            <Row >
                                <Col xs={12} className='align-right '>
                                    <Button className='me-4 mt-0 mb-0 ' variant="outline-secondary" size="md" style={{ borderRadius: 5, marginTop: 30 }}>
                                        <FaChevronLeft style={{ fontSize: 20, marginBottom: '2px', marginRight: '5px' }} /> Volver
                                    </Button>
                                    <Button type="submit" className=' btn-olt mt-0 mb-0' variant="primary" size="md" style={{ borderRadius: 5, marginTop: 30 }} >
                                        <FaSave style={{ fontSize: 20, marginBottom: '2px', marginRight: '5px' }} /> Guardar
                                    </Button>{' '}
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </>
    )
}

export default AddModelOnu
